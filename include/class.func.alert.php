<?php

class funcAlert {

  static function clear($level = 0) {
    $GLOBALS['app.alerts'] = array();
    $_SESSION['app.alerts'] = array();
  }

  static function get($level = 0, $html = true) {

    if (empty($GLOBALS['app.alerts']) || !is_array($GLOBALS['app.alerts']) || count($GLOBALS['app.alerts']) == 0) {
      return null;
    }
    if ($html) {
      $result = '';
    }
    else {
      $result = array();
    }
    foreach ($GLOBALS['app.alerts'] as $a) {
      if ($level == $GLOBALS['app.alert.all'] || $level == $a['level']) {
        if ($html) {
          $result .= "<script type='text/javascript'>";
          $result .= "window.onload = function() {";
          $result .= "$.growl({ title : '' , message :  '<span class=\"ui-icon " . $GLOBALS['app.alert.' . $GLOBALS['app.alert.' . $a['level']] . '.icon'] . "\" style=\"float: left; margin-right: .3em;\">&nbsp;</span><strong>" . $GLOBALS['app.alert.' . $GLOBALS['app.alert.' . $a['level']] . '.title'] . "</strong>: <span style=\" white-space: nowrap;\">" . $a['text'] . "</span>', style : '" . $GLOBALS['app.alert.' . $a['level']] . "' , size : \"large\"})";
          $result .= "};";
          $result .= "</script>";
        }
        else {
          $result[] = $a;
        }
      }
    }
    return $result;
  }

  private static function exists($text, $level, $alertArray) {
    if (is_array($alertArray) && count($alertArray) > 0) {
      foreach ($alertArray as $a) {
        if ($a['text'] == $text && $a['level'] == $level) {
          return true;
        }
      }
    }
    return false;
  }

  /**
   * $level = warning, error or success
   * $level defaults to warning
   * Will not add duplicates
   **/
  static function add($text, $level = null, $persistant = false) {
    if (empty($text)) {
      return false;
    }
    if (is_null($level)) {
      $level = $GLOBALS['app.alert.warning'];
    }
    $alert = array(
      'text' => $text,
      'level' => $level
    );
    if (!funcAlert::exists($alert['text'], $alert['level'], $GLOBALS['app.alerts'])) {
      $GLOBALS['app.alerts'][] = $alert;
    }
    if ($persistant && !empty($GLOBALS['app.alerts']) && !funcAlert::exists($alert['text'], $alert['level'], $_SESSION['app.alerts'])) {
      if (!isset($_SESSION['app.alerts'])) {
        $_SESSION['app.alerts'] = array();
      }
      $_SESSION['app.alerts'][] = $alert;
    }
    return true;
  }

}

?>