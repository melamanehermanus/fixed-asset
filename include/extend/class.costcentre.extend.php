<?php
class CostCentre extends _CostCentre {

  static function getName($costCentreId) {
    if (!empty($costCentreId)) {
      $sql = "SELECT `cost_centre` FROM `cost_centre` WHERE `idcost_centre` = {$costCentreId}";
      $result = $GLOBALS['app.db']->executeQuery($sql, true);
      if(!empty($result)) {
        return $result[0]['cost_centre'];
      }
    }
    return false;
  }

  static function getIdFromName($costCentre) {
    if (!empty($costCentre)) {
      $sql = "SELECT `idcost_centre` FROM `cost_centre` WHERE `cost_centre` LIKE '{$costCentre}'";
      $result = $GLOBALS['app.db']->executeQuery($sql, true);
      if(!empty($result)) {
        return $result[0]['idcost_centre'];
      }
    }
    return false;
  }

}
?>