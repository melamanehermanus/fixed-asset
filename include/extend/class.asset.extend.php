<?php
class Asset extends _Asset {
  public static function getCompleteAssets($locationId = null, $barcode = null, $searchSql = null) {
    $assetsSql = Asset::sql(null, (!empty($barcode) ? "`asset_barcode` = {$barcode}" : "`idlocation` = {$locationId}") . " {$searchSql}");
    if(!empty($locationId) || !empty($barcode)) {
      $where = array();
      if($GLOBALS['app.user']->IduserTitle != 5) {
        $where[] = "`identity` = {$GLOBALS['app.user']->Identity}";
      }
      if(!empty($locationId)) {
        $where[] = "`idlocation` = {$locationId}";
      }
      if(!empty($barcode)) {
        $where[] = "`asset_barcode` = {$barcode}";
      }
      if(!empty($searchSql)) {
        $where[] = "$searchSql";
      }
      $where = implode(' AND ', $where);

      $sql = "SELECT `asset`.* ,user.user_name, `identity`, `cost_centre`.`cost_centre`, `condition`.`condition_name` , status.status_name 
            FROM `asset` 
            INNER JOIN `user` ON `asset`.`iduser` = `user`.`iduser`
            INNER JOIN `cost_centre` ON `asset`.`idcost_centre` = `cost_centre`.`idcost_centre`
            INNER JOIN `condition` ON `asset`.`idcondition` = `condition`.`idcondition`
            INNER JOIN `status` ON `asset`.`idstatus` = `status`.`idstatus` 
            WHERE {$where}";
      return $GLOBALS['app.db']->executeQuery($sql, true);

    }
    return false;
  }
}
?>