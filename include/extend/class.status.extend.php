<?php
class Status extends _Status {

  static function getName($statusId) {
    if (!empty($statusId)) {
      $sql = "SELECT `status_name` FROM `status` WHERE `idstatus` = {$statusId}";
      $result = $GLOBALS['app.db']->executeQuery($sql, true);
      if(!empty($result)) {
        return $result[0]['status_name'];
      }
    }
    return false;
  }

  static function getIdFromName($status) {
    if (!empty($status)) {
      $sql = "SELECT `idstatus` FROM `status` WHERE `status_name` LIKE '{$status}'";
      $result = $GLOBALS['app.db']->executeQuery($sql, true);
      if(!empty($result)) {
        return $result[0]['idstatus'];
      }
    }
    return false;
  }

}
?>