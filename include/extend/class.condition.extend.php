<?php
class Condition extends _Condition {

  static function getName($conditionId) {
    if (!empty($conditionId)) {
      $sql = "SELECT `condition_name` FROM `condition` WHERE `idcondition` = {$conditionId}";
      $result = $GLOBALS['app.db']->executeQuery($sql, true);
      if(!empty($result)) {
        return $result[0]['condition_name'];
      }
    }
    return false;
  }

  static function getIdFromName($condition) {
    if (!empty($condition)) {
      $sql = "SELECT `idcondition` FROM `condition` WHERE `condition_name` LIKE '{$condition}'";
      $result = $GLOBALS['app.db']->executeQuery($sql, true);
      if(!empty($result)) {
        return $result[0]['idcondition'];
      }
    }
    return false;
  }

}
?>