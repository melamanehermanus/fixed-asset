<?php
class Location extends _Location {

  static function getName($locationId) {
    if (!empty($locationId)) {
      $sql = "SELECT `location_name` FROM location WHERE `idlocation` = {$locationId}";
      $result = $GLOBALS['app.db']->executeQuery($sql, true);
      if(!empty($result)) {
        return $result[0]['location_name'];
      }
    }
    return false;
  }

}
?>