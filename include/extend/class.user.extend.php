<?php
class User extends _User {

  static function encryptPassword($password) {
    if (!empty($password)) {
      if (!empty($GLOBALS['app.var.user.password.encryption']) && !empty($GLOBALS['app.var.user.password.salt'])) {
        $salt = $GLOBALS['app.var.user.password.salt'];
      }
      if ($GLOBALS['app.var.user.password.encryption'] == 'sha1') {
        $password = sha1($password . $salt);
      }
      elseif ($GLOBALS['app.var.user.password.encryption'] == 'md5') {
        $password = md5($password . $salt);
      }
    }
    return $password;
  }

  static function updateCurrentUserCredentials($username, $password) {
    $_SESSION[$GLOBALS['app.var.user.username']] = $username;
    $_SESSION[$GLOBALS['app.var.user.password']] = $password;
    $GLOBALS['app.user']->lookup();
  }

  static function getName($userId) {
    if (!empty($userId)) {
      $sql = "SELECT `user_name` FROM `user` WHERE `iduser` = {$userId}";
      $result = $GLOBALS['app.db']->executeQuery($sql, true);
      if(!empty($result)) {
        return $result[0]['user_name'];
      }
    }
    return false;
  }

}
?>