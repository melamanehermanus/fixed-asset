<?php
require_once($GLOBALS['app.folder.include'] . 'class.db.mysql.php');
$filename = $GLOBALS['app.folder.include.extend'] . 'class.category.extend.php';

if (is_file($filename)) {
  require_once($filename);
}
else {
  class Category extends _Category {
  }
}

class _Category {
  public $CategoryName;
  public $Idcategory;

  public function __construct($CategoryName = null) {
    if (!is_null($CategoryName)) {
      $this->lookup($CategoryName);
    }
  }

  private function getPrimaryKey($classOrDB = 'class') {
    if (strtolower($classOrDB) == 'class') {
      return 'CategoryName';
    }
    elseif (strtolower($classOrDB) == 'db') {
      return 'category_name';
    }
    return null;
  }

  public function exists($CategoryName = null, $Idcategory = null) {
    $CategoryName = (!empty($CategoryName)) ? $GLOBALS['app.db']->realEscapeString($CategoryName) : $this->CategoryName;
    $Idcategory = (!empty($Idcategory)) ? $GLOBALS['app.db']->realEscapeString($Idcategory) : $this->Idcategory;
    if (empty($CategoryName)) {
      return false;
    }
    if (empty($Idcategory)) {
      return false;
    }
    $sql = "SELECT `category_name` FROM category WHERE 
`category_name` = '" . $GLOBALS['app.db']->realEscapeString($this->CategoryName) . "' AND
`idcategory` = '" . $GLOBALS['app.db']->realEscapeString($this->Idcategory) . "'";
    $rows = $GLOBALS['app.db']->executeQuery($sql, true);
    $result = (isset($rows) && $rows) ? true : false;
    return $result;
  }

  public function lookup($value = null) {
    if (!is_array($value)) {
      $value = (!is_null($value)) ? $GLOBALS['app.db']->realEscapeString($value) : $this->CategoryName;
      if (is_null($value) || $value == '') {
        return false;
      }
      $field = 'category_name';
      $value = array($field => $value);
    }
    elseif (count($value) == 0) {
      return false;
    }

    $where = array();
    foreach ($value as $field => $v) {
      $field = $GLOBALS['app.db']->realEscapeString($field);
      if (is_null($v)) {
        $v = 'IS NULL';
      }
      else {
        $v = '= \'' . $GLOBALS['app.db']->realEscapeString($v) . '\'';
      }
      $where[] = "`$field` $v";
    }
    $where = implode(' AND ', $where);

    $sql = "SELECT * FROM category WHERE $where LIMIT 1";
    $rows = $GLOBALS['app.db']->executeQuery($sql, true);
    if (isset($rows) && $rows) {
      $this->populate($rows[0]);
      return true;
    }
    return false;
  }

  public static function get($select = null, $where = null, $order = null, $join = null, $page = null, $rpp = null, $countOnly = false, $sqlOnly = false, $arrayInsteadOfClass = false) {
    $select = (empty($select)) ? '`category`.*' : $select;
    $where = (empty($where)) ? '' : "WHERE $where";
    $order = (empty($order)) ? '' : 'ORDER BY ' . $order;
    $join = (empty($join)) ? '' : $join;
    $page = (empty($page)) ? 1 : abs((int)$page);
    $limit = (is_numeric($rpp)) ? 'LIMIT ' . ($page - 1) * $rpp . ',' . $rpp : '';
    if ($countOnly) {
      $sql = "SELECT COUNT(DISTINCT `category`.`category_name`) AS `Total` FROM category $join $where";
    }
    else {
      $sql = "SELECT $select FROM category $join $where $order $limit";
    }
    if ($sqlOnly) {
      return $sql;
    }
    $rows = $GLOBALS['app.db']->executeQuery($sql, true);
    if (isset($rows) && $rows) {
      if ($arrayInsteadOfClass) {
        return $rows;
      }
      if ($countOnly) {
        return $rows[0]['Total'];
      }
      $result = Array();
      foreach ($rows as $row) {
        $c = new Category();
        $c->populate($row);
        $result[] = $c;
      }
      return $result;
    }
    return null;
  }

  public static function count($where = null, $join = null) {
    return self::get(null, $where, null, $join, null, null, true);
  }

  public static function sql($select = null, $where = null, $order = null, $join = null, $page = null, $rpp = null, $countOnly = null) {
    return self::get($select, $where, $order, $join, $page, $rpp, $countOnly, true);
  }

  function populate($row) {
    $this->CategoryName = isset($row['category_name']) ? funcString::latinToUtf8($row['category_name']) : null;
    $this->Idcategory = isset($row['idcategory']) ? funcString::latinToUtf8($row['idcategory']) : null;
  }

  public function save() {
    if ($this->exists()) {
      $sql = "UPDATE category SET
`category_name` = " . funcData::nz($GLOBALS['app.db']->realEscapeString(funcString::utf8ToLatin($this->CategoryName)), false, true) . ",
`idcategory` = " . funcData::nz($GLOBALS['app.db']->realEscapeString($this->Idcategory), false, false) . "
WHERE
`category_name` = '" . $GLOBALS['app.db']->realEscapeString($this->CategoryName) . "' AND
`idcategory` = '" . $GLOBALS['app.db']->realEscapeString($this->Idcategory) . "'";
    }
    else {
      $sql = "INSERT INTO category SET
`category_name` = " . funcData::nz($GLOBALS['app.db']->realEscapeString(funcString::utf8ToLatin($this->CategoryName)), false, true) . ",
`idcategory` = " . funcData::nz($GLOBALS['app.db']->realEscapeString($this->Idcategory), false, false) . "";
    }
    $result = $GLOBALS['app.db']->executeSQL($sql);
    $result = ($result == 1) ? true : false;
    $this->CategoryName = (empty($this->CategoryName) && $result) ? $GLOBALS['app.db']->lastInsertId() : $this->CategoryName;
    return $result;
  }

  public function delete($CategoryName = null) {
    $CategoryName = (!empty($CategoryName)) ? $GLOBALS['app.db']->realEscapeString($CategoryName) : $this->CategoryName;
    if (empty($CategoryName)) {
      return false;
    }
    $sql = "DELETE FROM category WHERE `category_name` = '$CategoryName' LIMIT 1";
    $result = $GLOBALS['app.db']->executeSQL($sql);
    return ($result == 1) ? true : false;
  }

}
?>