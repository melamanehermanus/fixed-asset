<?php
require_once($GLOBALS['app.folder.include'] . 'class.db.mysql.php');
$filename = $GLOBALS['app.folder.include.extend'] . 'class.user.extend.php';

if (is_file($filename)) {
  require_once($filename);
}
else {
  class User extends _User {
  }
}

class _User {
  public $Iduser;
  public $Identity;
  public $IduserTitle;
  public $UserName;
  public $Password;
  public $Email;
  public $ContactNumber;

  public function __construct($Iduser = null) {
    if (!is_null($Iduser)) {
      $this->lookup($Iduser);
    }
  }

  private function getPrimaryKey($classOrDB = 'class') {
    if (strtolower($classOrDB) == 'class') {
      return 'Iduser';
    }
    elseif (strtolower($classOrDB) == 'db') {
      return 'iduser';
    }
    return null;
  }

  public function exists($Iduser = null) {
    $Iduser = (!empty($Iduser)) ? $GLOBALS['app.db']->realEscapeString($Iduser) : $this->Iduser;
    if (empty($Iduser)) {
      return false;
    }

    $sql = "SELECT `iduser` FROM user WHERE 
`iduser` = '" . $GLOBALS['app.db']->realEscapeString($Iduser) . "'";

    $rows = $GLOBALS['app.db']->executeQuery($sql, true);
    $result = (isset($rows) && $rows) ? true : false;
    return $result;
  }

  public function lookup($value = null) {
    if (!is_array($value)) {
      $value = (!is_null($value)) ? $GLOBALS['app.db']->realEscapeString($value) : $this->Iduser;
      if (is_null($value) || $value == '') {
        return false;
      }
      $field = 'iduser';
      $value = array($field => $value);
    }
    elseif (count($value) == 0) {
      return false;
    }

    $where = array();
    foreach ($value as $field => $v) {
      $field = $GLOBALS['app.db']->realEscapeString($field);
      if (is_null($v)) {
        $v = 'IS NULL';
      }
      else {
        $v = '= \'' . $GLOBALS['app.db']->realEscapeString($v) . '\'';
      }
      $where[] = "`$field` $v";
    }
    $where = implode(' AND ', $where);

    $sql = "SELECT * FROM user WHERE $where LIMIT 1";
    $rows = $GLOBALS['app.db']->executeQuery($sql, true);
    if (isset($rows) && $rows) {
      $this->populate($rows[0]);
      return true;
    }
    return false;
  }

  public static function get($select = null, $where = null, $order = null, $join = null, $page = null, $rpp = null, $countOnly = false, $sqlOnly = false, $arrayInsteadOfClass = false) {
    $select = (empty($select)) ? '`user`.*' : $select;
    $where = (empty($where)) ? '' : "WHERE $where";
    $order = (empty($order)) ? '' : 'ORDER BY ' . $order;
    $join = (empty($join)) ? '' : $join;
    $page = (empty($page)) ? 1 : abs((int)$page);
    $limit = (is_numeric($rpp)) ? 'LIMIT ' . ($page - 1) * $rpp . ',' . $rpp : '';
    if ($countOnly) {
      $sql = "SELECT COUNT(DISTINCT `user`.`iduser`) AS `Total` FROM user $join $where";
    }
    else {
      $sql = "SELECT $select FROM user $join $where $order $limit";
    }
    if ($sqlOnly) {
      return $sql;
    }
    $rows = $GLOBALS['app.db']->executeQuery($sql, true);
    if (isset($rows) && $rows) {
      if ($arrayInsteadOfClass) {
        return $rows;
      }
      if ($countOnly) {
        return $rows[0]['Total'];
      }
      $result = Array();
      foreach ($rows as $row) {
        $u = new User();
        $u->populate($row);
        $result[$u->Iduser] = $u;
      }
      return $result;
    }
    return null;
  }

  public static function count($where = null, $join = null) {
    return self::get(null, $where, null, $join, null, null, true);
  }

  public static function sql($select = null, $where = null, $order = null, $join = null, $page = null, $rpp = null, $countOnly = null) {
    return self::get($select, $where, $order, $join, $page, $rpp, $countOnly, true);
  }

  function populate($row) {
    $this->Iduser = isset($row['iduser']) ? funcString::latinToUtf8($row['iduser']) : null;
    $this->Identity = isset($row['identity']) ? funcString::latinToUtf8($row['identity']) : null;
    $this->IduserTitle = isset($row['iduser_title']) ? funcString::latinToUtf8($row['iduser_title']) : null;
    $this->UserName = isset($row['user_name']) ? funcString::latinToUtf8($row['user_name']) : null;
    $this->Password = isset($row['password']) ? funcString::latinToUtf8($row['password']) : null;
    $this->Email = isset($row['email']) ? funcString::latinToUtf8($row['email']) : null;
    $this->ContactNumber = isset($row['contact_number']) ? funcString::latinToUtf8($row['contact_number']) : null;
  }

  public function save() {
    if ($this->exists()) {
      $sql = "UPDATE user SET
`identity` = " . funcData::nz($GLOBALS['app.db']->realEscapeString($this->Identity), true, false) . ",
`iduser_title` = " . funcData::nz($GLOBALS['app.db']->realEscapeString($this->IduserTitle), true, false) . ",
`user_name` = " . funcData::nz($GLOBALS['app.db']->realEscapeString(funcString::utf8ToLatin($this->UserName)), true, true) . ",
`password` = " . funcData::nz($GLOBALS['app.db']->realEscapeString(funcString::utf8ToLatin($this->Password)), true, true) . ",
`email` = " . funcData::nz($GLOBALS['app.db']->realEscapeString(funcString::utf8ToLatin($this->Email)), true, true) . ",
`contact_number` = " . funcData::nz($GLOBALS['app.db']->realEscapeString(funcString::utf8ToLatin($this->ContactNumber)), true, true) . "
WHERE
`iduser` = '" . $GLOBALS['app.db']->realEscapeString($this->Iduser) . "'";
    }
    else {
      $sql = "INSERT INTO user SET
`identity` = " . funcData::nz($GLOBALS['app.db']->realEscapeString($this->Identity), true, false) . ",
`iduser_title` = " . funcData::nz($GLOBALS['app.db']->realEscapeString($this->IduserTitle), true, false) . ",
`user_name` = " . funcData::nz($GLOBALS['app.db']->realEscapeString(funcString::utf8ToLatin($this->UserName)), true, true) . ",
`password` = " . funcData::nz($GLOBALS['app.db']->realEscapeString(funcString::utf8ToLatin($this->Password)), true, true) . ",
`email` = " . funcData::nz($GLOBALS['app.db']->realEscapeString(funcString::utf8ToLatin($this->Email)), true, true) . ",
`contact_number` = " . funcData::nz($GLOBALS['app.db']->realEscapeString(funcString::utf8ToLatin($this->ContactNumber)), true, true) . "";
    }
    $result = $GLOBALS['app.db']->executeSQL($sql);
    $result = ($result == 1) ? true : false;
    $this->Iduser = (empty($this->Iduser) && $result) ? $GLOBALS['app.db']->lastInsertId() : $this->Iduser;
    return $result;
  }

  public function delete($Iduser = null) {
    $Iduser = (!empty($Iduser)) ? $GLOBALS['app.db']->realEscapeString($Iduser) : $this->Iduser;
    if (empty($Iduser)) {
      return false;
    }
    $sql = "DELETE FROM user WHERE `iduser` = '$Iduser' LIMIT 1";
    $result = $GLOBALS['app.db']->executeSQL($sql);
    return ($result == 1) ? true : false;
  }

}
?>