<?php
require_once($GLOBALS['app.folder.include'] . 'class.db.mysql.php');
$filename = $GLOBALS['app.folder.include.extend'] . 'class.costcentre.extend.php';

if (is_file($filename)) {
  require_once($filename);
}
else {
  class CostCentre extends _CostCentre {
  }
}

class _CostCentre {
  public $CostCentre;
  public $IdcostCentre;

  public function __construct($CostCentre = null) {
    if (!is_null($CostCentre)) {
      $this->lookup($CostCentre);
    }
  }

  private function getPrimaryKey($classOrDB = 'class') {
    if (strtolower($classOrDB) == 'class') {
      return 'CostCentre';
    }
    elseif (strtolower($classOrDB) == 'db') {
      return 'cost_centre';
    }
    return null;
  }

  public function exists($CostCentre = null, $IdcostCentre = null) {
    $CostCentre = (!empty($CostCentre)) ? $GLOBALS['app.db']->realEscapeString($CostCentre) : $this->CostCentre;
    $IdcostCentre = (!empty($IdcostCentre)) ? $GLOBALS['app.db']->realEscapeString($IdcostCentre) : $this->IdcostCentre;
    if (empty($CostCentre)) {
      return false;
    }
    if (empty($IdcostCentre)) {
      return false;
    }
    $sql = "SELECT `cost_centre` FROM cost_centre WHERE 
`cost_centre` = '" . $GLOBALS['app.db']->realEscapeString($this->CostCentre) . "' AND
`idcost_centre` = '" . $GLOBALS['app.db']->realEscapeString($this->IdcostCentre) . "'";
    $rows = $GLOBALS['app.db']->executeQuery($sql, true);
    $result = (isset($rows) && $rows) ? true : false;
    return $result;
  }

  public function lookup($value = null) {
    if (!is_array($value)) {
      $value = (!is_null($value)) ? $GLOBALS['app.db']->realEscapeString($value) : $this->CostCentre;
      if (is_null($value) || $value == '') {
        return false;
      }
      $field = 'cost_centre';
      $value = array($field => $value);
    }
    elseif (count($value) == 0) {
      return false;
    }

    $where = array();
    foreach ($value as $field => $v) {
      $field = $GLOBALS['app.db']->realEscapeString($field);
      if (is_null($v)) {
        $v = 'IS NULL';
      }
      else {
        $v = '= \'' . $GLOBALS['app.db']->realEscapeString($v) . '\'';
      }
      $where[] = "`$field` $v";
    }
    $where = implode(' AND ', $where);

    $sql = "SELECT * FROM cost_centre WHERE $where LIMIT 1";
    $rows = $GLOBALS['app.db']->executeQuery($sql, true);
    if (isset($rows) && $rows) {
      $this->populate($rows[0]);
      return true;
    }
    return false;
  }

  public static function get($select = null, $where = null, $order = null, $join = null, $page = null, $rpp = null, $countOnly = false, $sqlOnly = false, $arrayInsteadOfClass = false) {
    $select = (empty($select)) ? '`cost_centre`.*' : $select;
    $where = (empty($where)) ? '' : "WHERE $where";
    $order = (empty($order)) ? '' : 'ORDER BY ' . $order;
    $join = (empty($join)) ? '' : $join;
    $page = (empty($page)) ? 1 : abs((int)$page);
    $limit = (is_numeric($rpp)) ? 'LIMIT ' . ($page - 1) * $rpp . ',' . $rpp : '';
    if ($countOnly) {
      $sql = "SELECT COUNT(DISTINCT `cost_centre`.`cost_centre`) AS `Total` FROM cost_centre $join $where";
    }
    else {
      $sql = "SELECT $select FROM cost_centre $join $where $order $limit";
    }
    if ($sqlOnly) {
      return $sql;
    }
    $rows = $GLOBALS['app.db']->executeQuery($sql, true);
    if (isset($rows) && $rows) {
      if ($arrayInsteadOfClass) {
        return $rows;
      }
      if ($countOnly) {
        return $rows[0]['Total'];
      }
      $result = Array();
      foreach ($rows as $row) {
        $c = new CostCentre();
        $c->populate($row);
        $result[] = $c;
      }
      return $result;
    }
    return null;
  }

  public static function count($where = null, $join = null) {
    return self::get(null, $where, null, $join, null, null, true);
  }

  public static function sql($select = null, $where = null, $order = null, $join = null, $page = null, $rpp = null, $countOnly = null) {
    return self::get($select, $where, $order, $join, $page, $rpp, $countOnly, true);
  }

  function populate($row) {
    $this->CostCentre = isset($row['cost_centre']) ? funcString::latinToUtf8($row['cost_centre']) : null;
    $this->IdcostCentre = isset($row['idcost_centre']) ? funcString::latinToUtf8($row['idcost_centre']) : null;
  }

  public function save() {
    if ($this->exists()) {
      $sql = "UPDATE cost_centre SET
`cost_centre` = " . funcData::nz($GLOBALS['app.db']->realEscapeString(funcString::utf8ToLatin($this->CostCentre)), false, true) . ",
`idcost_centre` = " . funcData::nz($GLOBALS['app.db']->realEscapeString($this->IdcostCentre), false, false) . "
WHERE
`cost_centre` = '" . $GLOBALS['app.db']->realEscapeString($this->CostCentre) . "' AND
`idcost_centre` = '" . $GLOBALS['app.db']->realEscapeString($this->IdcostCentre) . "'";
    }
    else {
      $sql = "INSERT INTO cost_centre SET
`cost_centre` = " . funcData::nz($GLOBALS['app.db']->realEscapeString(funcString::utf8ToLatin($this->CostCentre)), false, true) . ",
`idcost_centre` = " . funcData::nz($GLOBALS['app.db']->realEscapeString($this->IdcostCentre), false, false) . "";
    }
    $result = $GLOBALS['app.db']->executeSQL($sql);
    $result = ($result == 1) ? true : false;
    $this->CostCentre = (empty($this->CostCentre) && $result) ? $GLOBALS['app.db']->lastInsertId() : $this->CostCentre;
    return $result;
  }

  public function delete($CostCentre = null) {
    $CostCentre = (!empty($CostCentre)) ? $GLOBALS['app.db']->realEscapeString($CostCentre) : $this->CostCentre;
    if (empty($CostCentre)) {
      return false;
    }
    $sql = "DELETE FROM cost_centre WHERE `cost_centre` = '$CostCentre' LIMIT 1";
    $result = $GLOBALS['app.db']->executeSQL($sql);
    return ($result == 1) ? true : false;
  }

}
?>