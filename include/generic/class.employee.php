<?php
require_once($GLOBALS['app.folder.include'] . 'class.db.mysql.php');
$filename = $GLOBALS['app.folder.include.extend'] . 'class.employee.extend.php';

if (is_file($filename)) {
  require_once($filename);
}
else {
  class Employee extends _Employee {
  }
}

class _Employee {
  public $Id;
  public $Number;
  public $Name;

  public function __construct($Id = null) {
    if (!is_null($Id)) {
      $this->lookup($Id);
    }
  }

  private function getPrimaryKey($classOrDB = 'class') {
    if (strtolower($classOrDB) == 'class') {
      return 'Id';
    }
    elseif (strtolower($classOrDB) == 'db') {
      return 'id';
    }
    return null;
  }

  public function exists($Id = null) {
    $Id = (!empty($Id)) ? $GLOBALS['app.db']->realEscapeString($Id) : $this->Id;
    if (empty($Id)) {
      return false;
    }

    $sql = "SELECT `id` FROM employee WHERE 
`id` = '" . $GLOBALS['app.db']->realEscapeString($Id) . "'";

    $rows = $GLOBALS['app.db']->executeQuery($sql, true);
    $result = (isset($rows) && $rows) ? true : false;
    return $result;
  }

  public function lookup($value = null) {
    if (!is_array($value)) {
      $value = (!is_null($value)) ? $GLOBALS['app.db']->realEscapeString($value) : $this->Id;
      if (is_null($value) || $value == '') {
        return false;
      }
      $field = 'id';
      $value = array($field => $value);
    }
    elseif (count($value) == 0) {
      return false;
    }

    $where = array();
    foreach ($value as $field => $v) {
      $field = $GLOBALS['app.db']->realEscapeString($field);
      if (is_null($v)) {
        $v = 'IS NULL';
      }
      else {
        $v = '= \'' . $GLOBALS['app.db']->realEscapeString($v) . '\'';
      }
      $where[] = "`$field` $v";
    }
    $where = implode(' AND ', $where);

    $sql = "SELECT * FROM employee WHERE $where LIMIT 1";
    $rows = $GLOBALS['app.db']->executeQuery($sql, true);
    if (isset($rows) && $rows) {
      $this->populate($rows[0]);
      return true;
    }
    return false;
  }

  public static function get($select = null, $where = null, $order = null, $join = null, $page = null, $rpp = null, $countOnly = false, $sqlOnly = false, $arrayInsteadOfClass = false) {
    $select = (empty($select)) ? '`employee`.*' : $select;
    $where = (empty($where)) ? '' : "WHERE $where";
    $order = (empty($order)) ? '' : 'ORDER BY ' . $order;
    $join = (empty($join)) ? '' : $join;
    $page = (empty($page)) ? 1 : abs((int)$page);
    $limit = (is_numeric($rpp)) ? 'LIMIT ' . ($page - 1) * $rpp . ',' . $rpp : '';
    if ($countOnly) {
      $sql = "SELECT COUNT(DISTINCT `employee`.`id`) AS `Total` FROM employee $join $where";
    }
    else {
      $sql = "SELECT $select FROM employee $join $where $order $limit";
    }
    if ($sqlOnly) {
      return $sql;
    }
    $rows = $GLOBALS['app.db']->executeQuery($sql, true);
    if (isset($rows) && $rows) {
      if ($arrayInsteadOfClass) {
        return $rows;
      }
      if ($countOnly) {
        return $rows[0]['Total'];
      }
      $result = Array();
      foreach ($rows as $row) {
        $e = new Employee();
        $e->populate($row);
        $result[$e->Id] = $e;
      }
      return $result;
    }
    return null;
  }

  public static function count($where = null, $join = null) {
    return self::get(null, $where, null, $join, null, null, true);
  }

  public static function sql($select = null, $where = null, $order = null, $join = null, $page = null, $rpp = null, $countOnly = null) {
    return self::get($select, $where, $order, $join, $page, $rpp, $countOnly, true);
  }

  function populate($row) {
    $this->Id = isset($row['id']) ? funcString::latinToUtf8($row['id']) : null;
    $this->Number = isset($row['number']) ? funcString::latinToUtf8($row['number']) : null;
    $this->Name = isset($row['name']) ? funcString::latinToUtf8($row['name']) : null;
  }

  public function save() {
    if ($this->exists()) {
      $sql = "UPDATE employee SET
`id` = " . funcData::nz($GLOBALS['app.db']->realEscapeString($this->Id), false, false) . ",
`number` = " . funcData::nz($GLOBALS['app.db']->realEscapeString(funcString::utf8ToLatin($this->Number)), true, true) . ",
`name` = " . funcData::nz($GLOBALS['app.db']->realEscapeString(funcString::utf8ToLatin($this->Name)), false, true) . "
WHERE
`id` = '" . $GLOBALS['app.db']->realEscapeString($this->Id) . "'";
    }
    else {
      $sql = "INSERT INTO employee SET
`id` = " . funcData::nz($GLOBALS['app.db']->realEscapeString($this->Id), false, false) . ",
`number` = " . funcData::nz($GLOBALS['app.db']->realEscapeString(funcString::utf8ToLatin($this->Number)), true, true) . ",
`name` = " . funcData::nz($GLOBALS['app.db']->realEscapeString(funcString::utf8ToLatin($this->Name)), false, true) . "";
    }
    $result = $GLOBALS['app.db']->executeSQL($sql);
    $result = ($result == 1) ? true : false;
    $this->Id = (empty($this->Id) && $result) ? $GLOBALS['app.db']->lastInsertId() : $this->Id;
    return $result;
  }

  public function delete($Id = null) {
    $Id = (!empty($Id)) ? $GLOBALS['app.db']->realEscapeString($Id) : $this->Id;
    if (empty($Id)) {
      return false;
    }
    $sql = "DELETE FROM employee WHERE `id` = '$Id' LIMIT 1";
    $result = $GLOBALS['app.db']->executeSQL($sql);
    return ($result == 1) ? true : false;
  }

}
?>