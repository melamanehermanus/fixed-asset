<?php
require_once($GLOBALS['app.folder.include'] . 'class.db.mysql.php');
$filename = $GLOBALS['app.folder.include.extend'] . 'class.condition.extend.php';

if (is_file($filename)) {
  require_once($filename);
}
else {
  class Condition extends _Condition {
  }
}

class _Condition {
  public $Idcondition;
  public $ConditionName;

  public function __construct($Idcondition = null) {
    if (!is_null($Idcondition)) {
      $this->lookup($Idcondition);
    }
  }

  private function getPrimaryKey($classOrDB = 'class') {
    if (strtolower($classOrDB) == 'class') {
      return 'Idcondition';
    }
    elseif (strtolower($classOrDB) == 'db') {
      return 'idcondition';
    }
    return null;
  }

  public function exists($Idcondition = null) {
    $Idcondition = (!empty($Idcondition)) ? $GLOBALS['app.db']->realEscapeString($Idcondition) : $this->Idcondition;
    if (empty($Idcondition)) {
      return false;
    }

    $sql = "SELECT `idcondition` FROM condition WHERE 
`idcondition` = '" . $GLOBALS['app.db']->realEscapeString($Idcondition) . "'";

    $rows = $GLOBALS['app.db']->executeQuery($sql, true);
    $result = (isset($rows) && $rows) ? true : false;
    return $result;
  }

  public function lookup($value = null) {
    if (!is_array($value)) {
      $value = (!is_null($value)) ? $GLOBALS['app.db']->realEscapeString($value) : $this->Idcondition;
      if (is_null($value) || $value == '') {
        return false;
      }
      $field = 'idcondition';
      $value = array($field => $value);
    }
    elseif (count($value) == 0) {
      return false;
    }

    $where = array();
    foreach ($value as $field => $v) {
      $field = $GLOBALS['app.db']->realEscapeString($field);
      if (is_null($v)) {
        $v = 'IS NULL';
      }
      else {
        $v = '= \'' . $GLOBALS['app.db']->realEscapeString($v) . '\'';
      }
      $where[] = "`$field` $v";
    }
    $where = implode(' AND ', $where);

    $sql = "SELECT * FROM condition WHERE $where LIMIT 1";
    $rows = $GLOBALS['app.db']->executeQuery($sql, true);
    if (isset($rows) && $rows) {
      $this->populate($rows[0]);
      return true;
    }
    return false;
  }

  public static function get($select = null, $where = null, $order = null, $join = null, $page = null, $rpp = null, $countOnly = false, $sqlOnly = false, $arrayInsteadOfClass = false) {
    $select = (empty($select)) ? '`condition`.*' : $select;
    $where = (empty($where)) ? '' : "WHERE $where";
    $order = (empty($order)) ? '' : 'ORDER BY ' . $order;
    $join = (empty($join)) ? '' : $join;
    $page = (empty($page)) ? 1 : abs((int)$page);
    $limit = (is_numeric($rpp)) ? 'LIMIT ' . ($page - 1) * $rpp . ',' . $rpp : '';
    if ($countOnly) {
      $sql = "SELECT COUNT(DISTINCT `condition`.`idcondition`) AS `Total` FROM condition $join $where";
    }
    else {
      $sql = "SELECT $select FROM condition $join $where $order $limit";
    }
    if ($sqlOnly) {
      return $sql;
    }
    $rows = $GLOBALS['app.db']->executeQuery($sql, true);
    if (isset($rows) && $rows) {
      if ($arrayInsteadOfClass) {
        return $rows;
      }
      if ($countOnly) {
        return $rows[0]['Total'];
      }
      $result = Array();
      foreach ($rows as $row) {
        $c = new Condition();
        $c->populate($row);
        $result[$c->Idcondition] = $c;
      }
      return $result;
    }
    return null;
  }

  public static function count($where = null, $join = null) {
    return self::get(null, $where, null, $join, null, null, true);
  }

  public static function sql($select = null, $where = null, $order = null, $join = null, $page = null, $rpp = null, $countOnly = null) {
    return self::get($select, $where, $order, $join, $page, $rpp, $countOnly, true);
  }

  function populate($row) {
    $this->Idcondition = isset($row['idcondition']) ? funcString::latinToUtf8($row['idcondition']) : null;
    $this->ConditionName = isset($row['condition_name']) ? funcString::latinToUtf8($row['condition_name']) : null;
  }

  public function save() {
    if ($this->exists()) {
      $sql = "UPDATE condition SET
`condition_name` = " . funcData::nz($GLOBALS['app.db']->realEscapeString(funcString::utf8ToLatin($this->ConditionName)), false, true) . "
WHERE
`idcondition` = '" . $GLOBALS['app.db']->realEscapeString($this->Idcondition) . "'";
    }
    else {
      $sql = "INSERT INTO condition SET
`condition_name` = " . funcData::nz($GLOBALS['app.db']->realEscapeString(funcString::utf8ToLatin($this->ConditionName)), false, true) . "";
    }
    $result = $GLOBALS['app.db']->executeSQL($sql);
    $result = ($result == 1) ? true : false;
    $this->Idcondition = (empty($this->Idcondition) && $result) ? $GLOBALS['app.db']->lastInsertId() : $this->Idcondition;
    return $result;
  }

  public function delete($Idcondition = null) {
    $Idcondition = (!empty($Idcondition)) ? $GLOBALS['app.db']->realEscapeString($Idcondition) : $this->Idcondition;
    if (empty($Idcondition)) {
      return false;
    }
    $sql = "DELETE FROM condition WHERE `idcondition` = '$Idcondition' LIMIT 1";
    $result = $GLOBALS['app.db']->executeSQL($sql);
    return ($result == 1) ? true : false;
  }

}
?>