<?php
funcCore::requireClasses('location');
$locations = funcArray::get($_POST, 'txtUpdateName');
$name = funcArray::get($_POST, 'txtName');
$deleteId = funcArray::get($_POST, 'deleteId');
$identity = funcArray::get($_POST, 'idEntity', $GLOBALS['app.user']->Identity);

//funcCore::redirect('home.php?module=locations', $identity . '$identity', $GLOBALS['app.alert.success']);

if (!empty($name)) {
  $building = funcArray::get($_POST, 'txtBuilding');
  $region = funcArray::get($_POST, 'txtRegion');
  $country = funcArray::get($_POST, 'txtCountry');
  $barcode = funcArray::get($_POST, 'txtBarcode');
  $longitude = funcArray::get($_POST, 'txtLongitude');
  $latitude = funcArray::get($_POST, 'txtLatitude');
//  $user = funcArray::get($_POST, 'ddUser');

  $sql = "select `idlocation`,`location_name` from `location` where `location_name` = '{$name}' and `building` ='{$building}' and `barcode` = '{$barcode}'";
  $existingEntity = $GLOBALS['app.db']->executeQuery($sql, true);
  if (empty($existingEntity)) {
    $sql = "INSERT INTO `location` SET `identity` = '{$identity}', `location_name` = '{$name}', `building` ='{$building}', `region` = '{$region}', `country` = '{$country}', `barcode` = '{$barcode}', `longitude` = '{$longitude}', `latitude` = '{$latitude}'";
    $result = $GLOBALS['app.db']->executeSQL($sql);
  }
  else {
    funcCore::redirect('home.php?module=locations', 'Error: This location already exists', $GLOBALS['app.alert.success']);
  }

}

if (!empty($locations)) {
  foreach ($locations as $key => $location) {
    $building = funcArray::get($_POST, 'txtUpdateBuilding');
    $region = funcArray::get($_POST, 'txtUpdateRegion');
    $country = funcArray::get($_POST, 'txtUpdateCountry');
    $barcode = funcArray::get($_POST, 'txtUpdateBarcode');
    $longitude = funcArray::get($_POST, 'txtUpdateLongitude');
    $latitude = funcArray::get($_POST, 'txtUpdateLatitude');
    $sql = "UPDATE `location` SET `location_name` = '{$location}', `building` ='{$building[$key]}', `region` = '{$region[$key]}', `country` = '{$country[$key]}', `barcode` = '{$barcode[$key]}', `longitude` = '{$longitude[$key]}', `latitude` = '{$latitude[$key]}' WHERE `idlocation` = {$key}";
    $result = $GLOBALS['app.db']->executeSQL($sql);
  }
}
if (!empty($deleteId)) {
  $sql = "DELETE FROM `location` WHERE `idlocation` = {$deleteId}";
  $result = $GLOBALS['app.db']->executeSQL($sql);
  funcCore::redirect('home.php?module=locations', 'Item successfully removed', $GLOBALS['app.alert.success']);
}

funcCore::redirect('home.php?module=locations', 'Saved successfully', $GLOBALS['app.alert.success']);

?>