<?php
funcCore::requireClasses('costcentre');
$newEntity = funcArray::get($_POST, 'txtAddEntity');
$entities = funcArray::get($_POST, 'txtUpdate');
$deleteId = funcArray::get($_POST, 'deleteId');
if (!empty($newEntity)) {
  $sql = "select `identity`,`entity_name` from `entity` where `entity_name` = '{$newEntity}'";
  $existingEntity = $GLOBALS['app.db']->executeQuery($sql, true);
  if (empty($existingEntity)) {
    $sql = "INSERT INTO `entity` SET `entity_name` = '{$newEntity}'";
    $result = $GLOBALS['app.db']->executeSQL($sql);
  }
  else {
    funcCore::redirect('home.php?module=entities', 'Error: This cost centre already exists', $GLOBALS['app.alert.success']);
  }

}

if (!empty($entities)) {
  foreach ($entities as $key => $entity) {
    $sql = "UPDATE `entity` SET `entity_name` = '{$entity}' WHERE `identity` = {$key}";
    $result = $GLOBALS['app.db']->executeSQL($sql);
  }
}
if (!empty($deleteId)) {
  $sql = "DELETE FROM `entity` WHERE `identity` = {$deleteId}";
  $result = $GLOBALS['app.db']->executeSQL($sql);
  funcCore::redirect('home.php?module=entities', 'Item successfully removed', $GLOBALS['app.alert.success']);
}

funcCore::redirect('home.php?module=entities', 'Saved successfully', $GLOBALS['app.alert.success']);

?>