<?php
funcCore::requireClasses('costcentre');
$newStatus = funcArray::get($_POST, 'txtAddStatus');
$statuses = funcArray::get($_POST, 'txtUpdate');
$deleteId = funcArray::get($_POST, 'deleteId');
if (!empty($newStatus)) {
  $sql = "select `idstatus`,`status_name` from `status` where `status_name` = '{$newStatus}'";
  $existingstatus = $GLOBALS['app.db']->executeQuery($sql, true);
  if (empty($existingstatus)) {
    $sql = "INSERT INTO `status` SET `status_name` = '{$newStatus}'";
    $result = $GLOBALS['app.db']->executeSQL($sql);
  }
  else {
    funcCore::redirect('home.php?module=status', 'Error: This cost centre already exists', $GLOBALS['app.alert.success']);
  }

}

if (!empty($statuses)) {
  foreach ($statuses as $key => $status) {
    $sql = "UPDATE `status` SET `status_name` = '{$status}' WHERE `idstatus` = {$key}";
    $result = $GLOBALS['app.db']->executeSQL($sql);
  }
}
if (!empty($deleteId)) {
  $sql = "DELETE FROM `status` WHERE `idstatus` = {$deleteId}";
  $result = $GLOBALS['app.db']->executeSQL($sql);
  funcCore::redirect('home.php?module=status', 'Item successfully removed', $GLOBALS['app.alert.success']);
}

funcCore::redirect('home.php?module=status', 'Saved successfully', $GLOBALS['app.alert.success']);

?>