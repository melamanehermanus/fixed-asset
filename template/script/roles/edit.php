<?php
/**
 * Created by PhpStorm.
 * User: ken13
 * Date: 11/9/2017
 * Time: 9:35 PM
 */
funcCore::requireClasses('roles');
$selectedPerms = funcArray::get($_POST, 'options');
$userRoleId = funcArray::get($_POST, 'roleId');
$modules = array(
  'Assets',
  'Locations',
  'Entity',
  'Category',
  'Status',
  'Cost Centre',
  'Conditions',
  'Import Data',
  'Users'
);
$moduleIndexes = array_keys($modules);
$selectedPermIndexes = array_keys($selectedPerms);
$deacticateModules = array_diff($moduleIndexes, $selectedPermIndexes);

if (!empty($selectedPerms)) {
  $permisions = array(
    0 => 'ViewRole',
    1 => 'AddRole',
    2 => 'EditRole',
    3 => 'DeleteRole'
  );
  foreach ($selectedPerms as $moduleId => $selectedPerm) {
    $role = new Roles(array(
      'RoleId' => $userRoleId,
      'Module' => $modules[$moduleId]
    ));
    foreach ($permisions as $key => $permision) {
      $role->{$permisions[$key]} = isset($selectedPerm[$key]) ? 1 : 0;
    }
    $role->save();
  }
  if (!empty($deacticateModules)) {
    foreach ($deacticateModules as $deacticateModule) {
      $role = new Roles(array(
        'RoleId' => $userRoleId,
        'Module' => $modules[$deacticateModule]
      ));
      foreach ($permisions as $key => $permision) {
        $role->{$permisions[$key]} = 0;
      }
      $role->save();
    }
  }
}
funcCore::redirect('home.php?module=roles&action=edit&id=' . $userRoleId, 'Permissions successfully updated', $GLOBALS['app.alert.success']);

