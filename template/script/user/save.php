<?php
funcCore::requireClasses('user');
$userId = funcArray::get($_REQUEST, 'id');
$user = new User($userId);

$user->Identity = funcArray::get($_POST, 'ddEntity');
$user->IduserTitle = funcArray::get($_POST, 'ddTitle');
$user->UserName = funcArray::get($_POST, 'UserName');
$password = funcArray::get($_POST, 'Password');
if (!empty($password)) {
  $user->Password = User::encryptPassword($password);
}

$user->ContactNumber = funcArray::get($_POST, 'ContactNumber');
$user->Email = funcArray::get($_POST, 'txtEmail');
$user->Type = funcArray::get($_POST, 'Type');

if ($user->save()) {
  if (!empty($password) && ($user->Iduser == $GLOBALS['app.user']->Iduser || $GLOBALS['app.user']->IduserTitle)) {
    User::updateCurrentUserCredentials($user->$GLOBALS['app.var.user.username'], $user->$GLOBALS['app.var.user.password']);
  }
  funcCore::redirect('home.php?module=user&action=modify&id=' . $user->Iduser, 'Saved successfully', $GLOBALS['app.alert.success']);
}
funcCore::redirect('home.php?module=user&action=modify', 'There was an error saving the user', $GLOBALS['app.alert.error']);
?>