<?php
funcCore::requireClasses('category');
$newCategory = funcArray::get($_POST, 'txtAddCategory');
$categories = funcArray::get($_POST, 'txtUpdate');
$deleteId = funcArray::get($_POST, 'deleteId');
if (!empty($newCategory)) {
  $sql = "select `idcategory`,`category_name` from `category` where `category_name` = '{$newCategory}'";
  $existingCategory = $GLOBALS['app.db']->executeQuery($sql, true);
  if (empty($existingCategory)) {
    $sql = "INSERT INTO `category` SET `category_name` = '{$newCategory}'";
    $result = $GLOBALS['app.db']->executeSQL($sql);
  }
  else {
    funcCore::redirect('home.php?module=category', 'Error: This cost centre already exists', $GLOBALS['app.alert.success']);
  }

}

if (!empty($categories)) {
  foreach ($categories as $key => $category) {
    $sql = "UPDATE `category` SET `category_name` = '{$category}' WHERE `idcategory` = {$key}";
    $result = $GLOBALS['app.db']->executeSQL($sql);
  }
}
if (!empty($deleteId)) {
  $sql = "DELETE FROM `category` WHERE `idcategory` = {$deleteId}";
  $result = $GLOBALS['app.db']->executeSQL($sql);
  funcCore::redirect('home.php?module=category', 'Item successfully removed', $GLOBALS['app.alert.success']);
}

funcCore::redirect('home.php?module=category', 'Saved successfully', $GLOBALS['app.alert.success']);

?>