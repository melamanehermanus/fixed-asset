<?php
funcCore::requireClasses('asset, entity, location, assetimage');
$locationId = funcArray::get($_REQUEST, 'locationId');
$barcode = funcArray::get($_REQUEST, 'barcode');

if(!empty($barcode) || !empty($locationId)) {
  $assets = Asset::get(null, (!empty($barcode) ? "`asset_barcode` = {$barcode}" : "`idlocation` = {$locationId}"));
  if(!empty($assets)) {
    $detailsTable = '
      <table class="table table-striped table-bordered table-hover table-responsive">
      <tr>
        <th>Action</th>
        <th>Location</th>
        <th>Barcode</th>
        <th>Asset Number</th>
        <th>Description</th>
        <th>Serial Number</th>
        <th>Last Location</th>
      </tr>';
      $assetId = 89;
    foreach ($assets as $asset) {
        $assetImage = AssetImage::get(null, "`idasset` = {$assetId}");
        $assetImage = funcArray::getFirstItem($assetImage);
        //<img src="' . substr($assetImage->AssetUrl, 1)  . '">
      $detailsTable .= '<tr id="asset' . $asset->Idasset . '" class="assetRow">
                          <td>
                          <div id="patientMedication">
                            <div>
                              <button type="button" class="btn btn-info" data-toggle="modal" data-target="#viewDetails">
                                View Details
                              </button>
                             </div>
                             <div id="viewDetails" style="margin-bottom:-20%" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="gridModalLabel" aria-hidden="true">
                              <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <button type="button" class="btn btn-default" data-dismiss="modal" style="float:right;">Close</button>
                                    </button>
                                    <h4 class="modal-title" id="gridModalLabel">Details</h4>
                                  </div>
                                  <div class="modal-body">
                                    
                                      <table style="width:100%;">
                                        <div style="">
                                            <img src="' . substr($assetImage->AssetUrl, 1)  . '" width="800px;" height="400px;">
                                        </div>
                                      </table>
                                   
                                  </div>
                                  <div class="modal-footer">
                                    
                                  </div>
                                </div>
                              </div>
                            </div>
                        </div></td>
                          <td>' . Location::getName($asset->Idlocation) . '</td>
                          <td>' . $asset->AssetBarcode . '</td>
                          <td>' . $asset->AssetNumber . '</td>
                          <td>' . $asset->AssetDescription . '</td>
                          <td>' . $asset->SerialNumber . '</td>
                          <td>' . (!empty($asset->Longitude) || !empty($asset->Longitude) ? $asset->Longitude . ' / ' . $asset->Latitude : null) . '</td>
                        </tr>';
    }
    $detailsTable .= '</table>';
    echo $detailsTable;
  }
}
exit;
?>

