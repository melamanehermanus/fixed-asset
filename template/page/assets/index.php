<?php
funcCore::requireClasses('asset, entity, location, assetimage, assettracker,condition,status,costcentre,category');
funcUI::queueScript('js', 'bottom', 'ext', 'js/jquery/jquery.js');
funcUI::queueScript('js', 'bottom', 'ext', 'js/jquery/jquery-ui.js');
funcUI::queueScript('js', 'bottom', 'ext', 'js/ui.script.js');
if (!isset($GLOBALS['app.var.user.permissions']['Assets']['view']) || $GLOBALS['app.var.user.permissions']['Assets']['view'] == 0) {
  funcCore::redirect('home.php?module=index&action=index', 'You do not have permission to view this page.', $GLOBALS['app.alert.Error']);
}

 $entityName = null;
if (!empty($GLOBALS['app.user']->Identity)) {
  $entityName = Entity::getName($GLOBALS['app.user']->Identity);
}

$entityOptions = funcArray::classesToSelectOptions(Entity::get(), 'Identity', 'EntityName');
$locationOptions = funcArray::classesToSelectOptions(Location::get(null, (!empty($GLOBALS['app.user']->Identity) ? "Identity = {$GLOBALS['app.user']->Identity}" : null)), 'Idlocation', 'LocationName');
$entityId = funcArray::get($_POST, 'ddEntity');
$locationId = funcArray::get($_POST, 'ddLocation');
$barcode = funcArray::get($_POST, 'txtBarcode');
$search = funcArray::get($_POST, 'txtsearch');
$category = funcArray::get($_POST, 'ddSearchCategory');
$condition = funcArray::get($_POST, 'ddSearchCondition');
$costCentre = funcArray::get($_POST, 'ddSearchCostCentre');
$status = funcArray::get($_POST, 'ddSearchStatus');
$statusOptions = funcArray::classesToSelectOptions(Status::get(), 'StatusName', 'StatusName');

$sql = "select * FROM `condition` ORDER BY `idcondition` ASC";
$conditions = $GLOBALS['app.db']->executeQuery($sql, true);
$conditionOptions = array();
if (!empty($conditions)) {
  foreach ($conditions as $searchCondition) {
    $conditionOptions[] = funcForm::prepOption($searchCondition['condition_name'], $searchCondition['condition_name']);
  }
}

$costCentreOptions = funcArray::classesToSelectOptions(CostCentre::get(null), 'IdcostCentre', 'CostCentre');
$categoryOptions = funcArray::classesToSelectOptions(Category::get(null), 'CategoryName', 'CategoryName');
$js = <<<JS
jQuery(document).ready(function($) {
    var entityId = $('#ddEntity').val();
    var locationId = $('#locationId').val();
     $.ajax({
        type: 'GET',
        url:'home.php?ajax&module=locations&action=getByEntity&entityId=' + entityId + '&locationId=' + locationId,
        success: function(result){

          $('#locationSpan').append(result);

        }
      });   
             
    $('#btnBack').click(function(e) {
      window.location = '?module=index';
    });

    $('#ddEntity').on("change", function(){

        $("#location_table tr").remove(); 
        $("#asset_table tr").remove();
        
        
        $('#locationSpan').children("span").remove();
        var entityId = $(this).val();

        $('ddEntity').prop('selectedIndex', 0);
        $.ajax({
          type: 'GET',
          url:'home.php?ajax&module=locations&action=getByEntity&entityId=' + entityId,
          success: function(result){

            $('#locationSpan').append(result);

          }
        });
    });
    $(document).on("change","#ddLocation",function(){
      var locationId = $(this).val();
       
      $.ajax({
        type: 'GET',
        url:'home.php?ajax&module=locations&action=getDetails&locationId=' + locationId,
        success: function(result){
                $("#location_table tr").remove();
                $("#asset_table tr").remove();
                $("#location_table").append(result);       
          }
      });
    });
    $('#btnGetAssets').on("click", function(){
      var barcode = $('#txtBarcode').val();
      var locationId = $('#ddLocation').val();
      if (barcode == '' && locationId == '') {
        alert('Please choose a location or enter a barcode');
        return false;
      }
    });
    
    $(document).on('keyup', '#txtSearch',function () {  
      alert("keyup");
      searchTable($(this).val());
    });    
    
    function searchTable(inputVal){
      var table = $('.viewUsersDiv table tbody');
      table.find('tr').each(function(index, row){
        var allCells = $(row).find('td');
        if(allCells.length > 0){
          var found = false;
          allCells.each(function(index, td){
            var regExp = new RegExp(inputVal, 'i');
            if(regExp.test($(td).text())){
              found = true;
              return false;
            }
          });
          if(found == true)$(row).show();else $(row).hide();
        }
      });
    }
});
JS;
funcUI::queueScript('js', 'bottom', 'embed', $js);

$content .= funcForm::form('frmAssests', 'post') . funcForm::hidden('locationId', $locationId);

//if (!empty($locationId)) {
//  if ($GLOBALS['app.var.site.mode'] === "super_user") {
//    $locationOptions = funcArray::classesToSelectOptions(Location::get(null, "`identity` = {$entityId}"), 'Idlocation', 'LocationName');
//  }
//  else {
//    $locationOptions = funcArray::classesToSelectOptions(Location::get(null, "`identity` = {$GLOBALS['app.var.site.identity']}"), 'Idlocation', 'LocationName');
//  }
//}
//echo funcArray::display($_POST);
//exit;
$content .= '<div id="page-wrapper" style="background-color: #F5F5F5">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Assets' . (!empty($entityName) ? ' (' . $entityName . ')' : null) . '</h1>
                        <!-- /.col-lg-12 -->
                    </div>
                </div>';
$content .= '

<h3>Select a Location or enter an Asset Barcode</h3>
<table style="margin-bottom: 20px;">
  <tr>';
if ($GLOBALS['app.user']->IduserTitle == 5) {
  $content .= '<td><span>Entity ' . funcForm::select('ddEntity', $entityId, 'Please select', $entityOptions, 'form-control') . '</span></td>';
  $content .= '<td style="padding-left: 10px;"><span id="locationSpan"></span></td>';
}
else {
  $content .= '<td style="padding-left: 10px;">Location' . funcForm::select('ddLocation', $locationId, 'Select a Location', $locationOptions, "form-control") . '</td>';
}
$content .= '<td style="padding-left: 10px;">Asset Barcode ' . funcForm::text('txtBarcode', null, "form-control") . '</span></td>
    <td style="padding-left: 10px;">&nbsp;' . funcForm::submit('btnGetAssets', 'Get Assets', "btn btn-primary form-control") . '</span></td>
  </tr>
</table>';


if (!empty($barcode) || !empty($locationId)) {
  $searchSql = array();
  if (!empty($search)) {
    $searchSql[] = "(`asset_description` LIKE '%{$search}%' OR `asset_barcode` LIKE '%{$search}%' OR `asset_number` LIKE '%{$search}%')";
  }
  if (!empty($category)) {
    $searchSql[] = "(`asset`.`category_name` LIKE '{$category}')";
  }
  if (!empty($condition)) {
    $searchSql[] = "(`condition_name` = '{$condition}')";
  }
  if (!empty($costCentre)) {
    $searchSql[] = "(`asset`.`idcost_centre` = {$costCentre})";
  }
  if (!empty($status)) {
    $searchSql[] = "(`status_name` = '{$status}')";
  }
  if (!empty($searchSql)) {
    $searchSql = implode(' AND ', $searchSql);
  }
  else {
    $searchSql = null;
  }

  $assets = Asset::getCompleteAssets($locationId, $barcode, $searchSql);
  if (!empty($assets)) {
    $content .= '
      <div style="margin: 10px 0 10px 0;">
        ' . funcForm::submit('btnExport', 'Export To Excel', 'btn btn-success') . '
      </div>';
  }

  if (!empty($locationId)) {
    $locationdetails = Location::get(null, "`idlocation` = {$locationId}");
    $locationdetails = funcArray::getFirstItem($locationdetails);
    $content .= '<div style="margin-top: 15px;">
    
    
      <table id="location_table" class="table table-striped table-bordered table-hover table-responsive">
        <tr style="">
          <th style="color: #FFFFFF;background-color: #15147b">Location Name</th>
          <th style="color: #FFFFFF;background-color: #15147b">Location Barcode Number</th>
          <th style="color: #FFFFFF;background-color: #15147b">Building</th>
          <th style="color: #FFFFFF;background-color: #15147b">Region</th>
          <th style="color: #FFFFFF;background-color: #15147b">Country</th>
          <th style="color: #FFFFFF;background-color: #15147b">Longitude</th>
          <th style="color: #FFFFFF;background-color: #15147b">Latitude</th>
        </tr>
        <tr>
            <td>' . $locationdetails->LocationName . '</td>
            <td>' . $locationdetails->Barcode . '</td>
            <td>' . $locationdetails->Building . '</td>
            <td>' . $locationdetails->Region . '</td>
            <td>' . $locationdetails->Country . '</td>
            <td>' . $locationdetails->Longitude . '</td>
            <td>' . $locationdetails->Latitude . '</td>
         </tr>
      </table>  
    </div>
    <div>
    </div>
    <div><h3>Assets </h3>
    <div style="margin-bottom: 15px;">Find assets that match keyword(s)<span style="margin-left: 5px;">' . funcForm::text('txtsearch', $search, null, null, null, "placeholder='Search Text'") . '</span></div>
    <table>
      <tr>
        <td style="padding-left: 5px;">Category</td><td>' . funcForm::select('ddSearchCategory', $category, 'Please Select', $categoryOptions, "form-control") . '</td>
        <td style="padding-left: 5px;">Condition</td><td>' . funcForm::select('ddSearchCondition', $condition, 'Please Select', $conditionOptions, "form-control") . '</td>
        <td style="padding-left: 5px;">Cost Centre</td><td>' . funcForm::select('ddSearchCostCentre', $costCentre, 'Please Select', $costCentreOptions, "form-control") . '</td>
        <td style="padding-left: 5px;">Status</td><td>' . funcForm::select('ddSearchStatus', $status, 'Please Select', $statusOptions, "form-control") . '</td>
        <td style="padding-left: 5px;">' . funcForm::submit('btnSearch', 'Search', "btn btn-primary form-control") . '</td>
      </tr>
    </table>
    </div><br />
    ';

  }
}
else {
  $content .= '
    <div style="margin-top: 15px;">
      <table id="location_table" class="table table-striped table-bordered table-hover table-responsive">
        <tr style="">
          <th style="color: #FFFFFF;background-color: #15147b">Location Name</th>
          <th style="color: #FFFFFF;background-color: #15147b">Location Barcode Number</th>
          <th style="color: #FFFFFF;background-color: #15147b">Building</th>
          <th style="color: #FFFFFF;background-color: #15147b">Region</th>
          <th style="color: #FFFFFF;background-color: #15147b">Country</th>
          <th style="color: #FFFFFF;background-color: #15147b">Longitude</th>
          <th style="color: #FFFFFF;background-color: #15147b">Latitude</th>
        </tr>
      </table>  
    </div>
    <div>
    </div><br />
    ';


}

if (isset($_POST['btnGetAssets']) || isset($_POST['btnExport']) || isset($_POST['btnSearch'])) {
//echo funcArray::display($assets);
//exit;
  $htmlTable = null;
  if (!empty($assets)) {
    $defaultImage = '/images/placeholder.png';
    $imageArray = array();
    $htmlTable .= '<div class="viewUsersDiv">
    <table id="asset_table" class="table table-blue-header table-striped table-bordered table-hover table-responsive">
    <tr>
      ' . (!isset($_POST['btnExport']) ? '<th excelformat="bg=#172357, fc=#FFFFFF", b, aw" style="color: #15147b;">Preview</th>' : '<th excelformat="bg=#172357, fc=#FFFFFF", b, aw">Image</th>') . '
      <th excelformat="bg=#172357, fc=#FFFFFF", b, aw" style="color: #15147b;">Description</th>
      <th excelformat="bg=#172357, fc=#FFFFFF", b, aw" style="color: #15147b;">Asset Barcode number</th>
      <th excelformat="bg=#172357, fc=#FFFFFF", b, aw" style="color: #15147b;">Asset Number</th>
      <th excelformat="bg=#172357, fc=#FFFFFF", b, aw" style="color: #15147b;">Serial Number</th>
      <th excelformat="bg=#172357, fc=#FFFFFF", b, aw" style="color: #15147b;">Value</th>
      <th excelformat="bg=#172357, fc=#FFFFFF", b, aw" style="color: #15147b;">Last Location</th>
      <th excelformat="bg=#172357, fc=#FFFFFF", b, aw" style="color: #15147b;">Category Name</th>
      <th excelformat="bg=#172357, fc=#FFFFFF", b, aw" style="color: #15147b;">Cost Center</th>
      <th excelformat="bg=#172357, fc=#FFFFFF", b, aw" style="color: #15147b;">Condition</th>
      <th excelformat="bg=#172357, fc=#FFFFFF", b, aw" style="color: #15147b;">Status</th>
      <th excelformat="bg=#172357, fc=#FFFFFF", b, aw" style="color: #15147b;">Cost</th>
      <th excelformat="bg=#172357, fc=#FFFFFF", b, aw" style="color: #15147b;">Supplier</th>
      <th excelformat="bg=#172357, fc=#FFFFFF", b, aw" style="color: #15147b;">Invoice</th>
      ' . (!isset($_POST['btnExport']) ? '<th style="color: #15147b;">Action</th>' : null) . '
    </tr>';
    foreach ($assets as $asset) {
      $assetId = $asset['idasset'];
      $assetsImage = AssetImage::get(null, "`idasset` = {$assetId}", "`image_date` DESC");
      $assetImage = funcArray::getFirstItem($assetsImage);
      if (!isset($_POST['btnExport'])) {
        $htmlTable .= '<tr id="asset' . $assetId . '" class="assetRow">
            <td><div style="">
                          <img src="' . (!empty($assetImage->AssetUrl) ? substr($assetImage->AssetUrl, 1) : $GLOBALS['app.ui.theme.folder'] . $defaultImage)  . '" style="width:80px; height:80px;">
                      </div></td>
                <td>' . $asset['asset_description'] . '</td>
                <td>' . $asset['asset_barcode'] . '</td>
                <td>' . $asset['asset_number'] . '</td>
                <td>' . $asset['serial_number'] . '</td>
                <td>' . (!empty($asset['value']) ? $asset['value'] : null) . '</td>
                <td>' . (!empty($asset['longitude']) || !empty($asset['longitude']) ? $asset['longitude'] . ' / ' . $asset['latitude'] : null) . '</td>
                <td>' . $asset['category_name'] . '</td>
                <td>' . $asset['cost_centre'] . '</td>
                <td>' . $asset['condition_name'] . '</td>
                <td>' . $asset['status_name'] . '</td>
                <td>' . $asset['cost'] . '</td>
                <td>' . $asset['supplier'] . '</td>
                <td>' . $asset['invoice'] . '</td>';


        $htmlTable .= '<td>
                        <div id="patientMedication">
                          <div>
                            <button type="button" class="btn btn-info" data-toggle="modal" data-target="#viewDetails' . $assetId . '">
                              View History
                            </button>
                           </div>
                           <div id="viewDetails' . $assetId . '" style="margin-bottom:20%; width:100%" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="gridModalLabel" aria-hidden="true">
                            <div style="width:90%"  class="modal-dialog modal-lg" role="document">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                      <button type="button" class="btn btn-default" data-dismiss="modal" style="float:right;">Close</button>
                                  </button>
                                  <h4 class="modal-title" id="gridModalLabel">Details</h4>
                                </div>
                                <div class="modal-body">
                                ';
        $htmlTableModal = null;
        $htmlTableModal .= '<table style="width:100%;"  class="table table-blue-header table-striped table-bordered table-hover table-responsive">';

        $fixedAssets = AssetTracker::get(null, "`idasset` = {$assetId}");
        if (!empty($fixedAssets)) {
          $htmlTableModal .= '<tr>
                                                    <th excelformat="bg=#172357, fc=#FFFFFF", b" style="color: #15147b;">Asset Number</th>
                                                    <th excelformat="bg=#172357, fc=#FFFFFF", b" style="color: #15147b;">Location Name</th>
                                                    <th excelformat="bg=#172357, fc=#FFFFFF", b" style="color: #15147b;">Category Name</th>
                                                    <th excelformat="bg=#172357, fc=#FFFFFF", b" style="color: #15147b;">Description</th>
                                                    <th excelformat="bg=#172357, fc=#FFFFFF", b" style="color: #15147b;">Make/Model</th>
                                                    <th excelformat="bg=#172357, fc=#FFFFFF", b" style="color: #15147b;">Serial No</th>
                                                    <th excelformat="bg=#172357, fc=#FFFFFF", b, aw" style="color: #15147b;">Category Name</th>
                                                    <th excelformat="bg=#172357, fc=#FFFFFF", b, aw" style="color: #15147b;">Cost Center</th>
                                                    <th excelformat="bg=#172357, fc=#FFFFFF", b, aw" style="color: #15147b;">Condition</th>
                                                    <th excelformat="bg=#172357, fc=#FFFFFF", b, aw" style="color: #15147b;">Status</th>
                                                    <th excelformat="bg=#172357, fc=#FFFFFF", b, aw" style="color: #15147b;">User</th>
                                                    <th excelformat="bg=#172357, fc=#FFFFFF", b" style="color: #15147b;">Date</th>
                                                    <th excelformat="bg=#172357, fc=#FFFFFF", b" style="color: #15147b;">Cost</th>
                                                    <th excelformat="bg=#172357, fc=#FFFFFF", b" style="color: #15147b;">Supplier</th>
                                                    <th excelformat="bg=#172357, fc=#FFFFFF", b" style="color: #15147b;">Invoice</th>
                                                    
                                                   </tr>';
          $assetHistory = true;
          foreach ($fixedAssets as $fixedAsset) {
            $assetUser = new User($fixedAsset->Iduser);
            $assetCostCentre = new CostCentre($fixedAsset->IdcostCentre);
            $condition = new Condition($fixedAsset->Idcondition);
            $currentAsset = new Asset($fixedAsset->Idasset);
            $htmlTableModal .= '<tr>
                                                        <td>' . $fixedAsset->AssetNumber . '</td>
                                                        <td>' . Location::getName($fixedAsset->Idlocation) . '</td>
                                                        <td>' . $fixedAsset->CategoryName . '</td>
                                                        <td>' . $fixedAsset->AssetDescription . '</td>
                                                        <td>' . $fixedAsset->Make . '<br>' . $fixedAsset->Model . '</td>
                                                        <td>' . $fixedAsset->SerialNumber . '</td>
                                                        <td>' . $fixedAsset->CategoryName . '</td>
                                                        <td>' . $assetCostCentre->CostCentre . '</td>
                                                        <td>' . $condition->ConditionName . '</td>
                                                        <td>' . $fixedAsset->SerialNumber . '</td>
                                                        <td>' . $assetUser->UserName . '</td>
                                                        <td>' . $fixedAsset->DateTime . '</td>
                                                        <td>' . $currentAsset->Cost . '</td>
                                                        <td>' . $currentAsset->Supplier . '</td>
                                                        <td>' . $currentAsset->Invoice . '</td>
                                                       </tr>';
            if ($assetHistory) {
              $assetHistory = false;
            }
          }
        }

        $htmlTableModal .= '</table>';
        $htmlTable .= $htmlTableModal;
//                    $htmlTable .= $htmlTableModalImages;
        $htmlTable .= '</div>
                                <div class="modal-footer">
                                  
                                </div>
                              </div>
                            </div>
                          </div>
                      </div>

                     <div id="patientMedication">
                          <div>
                            <button type="button" style="margin-top:20px;"  class="btn btn-info" data-toggle="modal" data-target="#viewImageDetails' . $assetId . '">
                              View Images
                            </button>
                           </div>
                           <div id="viewImageDetails' . $assetId . '" style="margin-bottom:5%; width:100%" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="gridModalLabel" aria-hidden="true">
                            <div style="width:30%"  class="modal-dialog modal-lg" role="document">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                      <button type="button" class="btn btn-default" data-dismiss="modal" style="float:right;">Close</button>
                                  </button>
                                  <h4 class="modal-title" id="gridModalLabel">Images</h4>
                                </div>
                                <div class="modal-body">
                                ';
        $htmlTableModalImages = null;
        $htmlTableModalImages .= '<table style="width:100%;"  class="table table-blue-header table-striped table-bordered table-hover table-responsive">';
        if (!empty($assetsImage)) {
          $htmlTableModalImages .= '<tr>
                                                    <th excelformat="bg=#172357, fc=#FFFFFF", b" style="color: #15147b;">Images</th>
                                                    
                                                   </tr>';
          $assetHistory = true;
          foreach ($assetsImage as $image) {
            $htmlTableModalImages .= '<tr>
                                                                <td>
                                                                <div style="">' . $image->ImageDate . '</div>
                                                                <div style="">
                                                                    <img src="' . substr($image->AssetUrl, 1) . '" style="max-width:100%; max-height:100%;">
                                                                </div></td>
                                                                
                                                            </tr>';
            if ($assetHistory) {
              $assetHistory = false;
            }
          }
        }

        $htmlTableModalImages .= '</table>';
        $htmlTable .= $htmlTableModalImages;
        $htmlTable .= '</div>
                                <div class="modal-footer">

                                </div>
                              </div>
                            </div>
                          </div>
                      </div>
                      </td>
                    </tr>';

      }
      else {
        /** Format excel export fields **/
        $imageUrl = $defaultImage;
        if (!empty($assetImage)) {
          $assetImage = funcArray::getFirstItem($assetsImage);
          $imageUrl = $assetImage->AssetUrl;
        }
        /** Collect images to display in excel **/
        $imageArray[] = $imageUrl;
        $htmlTable .= '<tr>
                <td></td>
                <td excelformat="aw">' . $asset['asset_description'] . '</td>
                <td excelformat="int,aw">' . $asset['asset_barcode'] . '</td>
                <td excelformat="int,aw">' . $asset['asset_number'] . '</td>
                <td excelformat="aw">' . $asset['serial_number'] . '</td>
                <td excelformat="int,aw">' . (isset($asset['value']) ? $asset['value'] : null) . '</td>
                <td excelformat="aw">' . (!empty($asset['longitude']) || !empty($asset['longitude']) ? $asset['longitude'] . ' / ' . $asset['latitude'] : null) . '</td>
                <td excelformat="aw">' . $asset['category_name'] . '</td>
                <td excelformat="aw">' . $asset['cost_centre'] . '</td>
                <td excelformat="aw">' . $asset['condition_name'] . '</td>
                <td excelformat="aw">' . $asset['status_name'] . '</td>
              </tr>';
      }

    }
    $htmlTable .= '</table></div>';
  }
  $content .= $htmlTable;
}

if (isset($_POST['btnExport'])) {
  $tabindex = 0;
  include $GLOBALS['app.folder.include'] . 'PHPExcel.php';
  include $GLOBALS['app.folder.include'] . 'PHPExcel/Writer/Excel5.php';

  $objPHPExcel = new PHPExcel();
  $objPHPExcel->getProperties()->setCreator("Facet Asset Management");
  $objPHPExcel->getProperties()->setLastModifiedBy("Facet Asset Management");
  $objPHPExcel->getProperties()->setTitle("Facet Asset Management");
  $objPHPExcel->getProperties()->setSubject("Facet Asset Management");
  $objPHPExcel->getProperties()->setDescription("Facet Asset Management");

  $objPHPExcel->setActiveSheetIndex($tabindex);
  $objPHPExcel->getActiveSheet()->setTitle('Asset Details');
  funcData::html2xls($htmlTable, $objPHPExcel, $tabindex, 80);

  /** Overlay images to excel sheet **/
  foreach ($imageArray as $key => $ia) {
//Add the image to the cell
    $cellNum = $key + 2;
    $objDrawing = new PHPExcel_Worksheet_Drawing();
    $objDrawing->setName('Asset Image');
    $objDrawing->setDescription('Asset Image');
    $objDrawing->setPath('.' . $ia);
    $objDrawing->setResizeProportional(true);
    $objDrawing->setWidth(60);
    $objDrawing->setCoordinates('A' . $cellNum);
    $objDrawing->setWorksheet($objPHPExcel->getActiveSheet());
  }

  header('Content-Type: application/vnd.ms-excel');
  header('Content-Disposition: attachment;filename="asset_details_' . urlencode(date('Y-m-d_H-i-s') . '.xls') . '"');
  header('Content-Transfer-Encoding: binary');
  header('Expires: 0');

  // check for IE only headers
  if (isset($_SERVER['HTTP_USER_AGENT']) && (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE') !== false)) {
    header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
    header('Pragma: public');
  }
  else {
    header('Pragma: no-cache');
  }

  $objWriter = new PHPExcel_Writer_Excel5($objPHPExcel);
  $objWriter->save('php://output');
  exit;
}
?>