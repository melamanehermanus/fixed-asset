<?php
/**
 * Created by PhpStorm.
 * User: ken13
 * Date: 11/5/2017
 * Time: 3:02 PM
 */
funcCore::requireClasses('assets, entity, location');
funcUI::queueScript('js', 'bottom', 'ext', 'js/jquery/jquery.js');
funcUI::queueScript('js', 'bottom', 'ext', 'js/jquery/jquery-ui.js');
funcUI::queueScript('js', 'bottom', 'ext', 'js/ui.script.js');
if (!isset($GLOBALS['app.var.user.permissions']['Import Data']['view']) || $GLOBALS['app.var.user.permissions']['Import Data']['view'] == 0) {
  funcCore::redirect('home.php?module=index&action=index', 'You do not have permission to view this page.', $GLOBALS['app.alert.Error']);
}

$entityOptions = funcArray::classesToSelectOptions(Entity::get(), 'Identity', 'EntityName');
$locationOptions = funcArray::classesToSelectOptions(Location::get(), 'Idlocation', 'LocationName');
$js = <<<JS
jQuery(document).ready(function($) {
  
    var entityId = $('#ddEntity').val();
     $.ajax({
        type: 'GET',
        url:'home.php?ajax&module=locations&action=getByEntity&entityId=' + entityId,
        success: function(result){

          $('#locationSpan').append(result);

        }
      });   
             
    $('#btnBack').click(function(e) {
      window.location = '?module=index';
    });

    $('#ddEntity').on("change", function(){

        $("#location_table tr").remove(); 
        $("#asset_table tr").remove();
        
        
        $('#locationSpan').children("span").remove();
        var entityId = $(this).val();

        $('ddEntity').prop('selectedIndex', 0);
        $.ajax({
          type: 'GET',
          url:'home.php?ajax&module=locations&action=getByEntity&entityId=' + entityId,
          success: function(result){

            $('#locationSpan').append(result);

          }
        });
    });
});
JS;
funcUI::queueScript('js', 'bottom', 'embed', $js);
//funcForm::validation('frmImport');
$entityName = null;
if (!empty($GLOBALS['app.user']->Identity)) {
  $entityName = Entity::getName($GLOBALS['app.user']->Identity);
}

$content .= funcForm::form('frmImport', 'post', null, true);
$entityId = null;
$content .= '
<div id="page-wrapper" style="background-color: #F5F5F5">
  <div class="row">
      <div class="col-lg-12">
          <h1 class="page-header">Import</h1>
      </div>
  </div>';

    $content .= '
<h3>' . ($GLOBALS['app.user']->IduserTitle == 5 ? 'Select an Entity and a Location' : 'Select a Location') . '</h3>
  <table style="margin-bottom: 20px;">
    <tr>';
if ($GLOBALS['app.user']->IduserTitle == 5){
  $content .= '
      <td><span>Entity ' . funcForm::select('ddEntity', $entityId, 'Please select', $entityOptions, 'validate[\'required\'] form-control') . '</span></td>
      <td style="padding-left: 10px;"><span id="locationSpan"></span></td>';
}
else {
  $locationOptions = funcArray::classesToSelectOptions(Location::get(null, "Identity = {$GLOBALS['app.user']->Identity}"), 'Idlocation', 'LocationName');
  $content .= '
      <td><span>Location ' . funcForm::select('ddLocation', null, 'Please select', $locationOptions, 'validate[\'required\'] form-control') . '</span></td>';
}


$content .= '
<td style="padding-left: 10px;">File' . funcForm::file('Filebrowse[]', 'validate[\'required\'] form-control', 'multiple style="width:100%"') . '</span>
<td style="padding-left: 10px;">&nbsp;' . funcForm::submit('btnImport', 'Import', "btn btn-primary form-control") . '</span></td>
    </tr>
  </table>
</div>' . funcForm::closeForm();

