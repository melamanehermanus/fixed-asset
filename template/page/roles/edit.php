<?php
/**
 * Created by PhpStorm.
 * User: ken13
 * Date: 11/7/2017
 * Time: 7:49 PM
 */
funcCore::requireClasses('usertitle,roles');
funcUI::queueScript('js', 'bottom', 'ext', 'js/jquery/jquery.js');
funcUI::queueScript('js', 'bottom', 'ext', 'js/jquery/jquery-ui.js');
funcUI::queueScript('js', 'bottom', 'ext', 'js/ui.script.js');
if ($GLOBALS['app.user']->IduserTitle != 5) {
  funcCore::redirect('home.php?module=index&action=index', 'You do not have permission to view this page.', $GLOBALS['app.alert.Error']);
}

$permisions = array(1 => 'View', 2 => 'Add', 3 => 'Edit', 4 => 'Delete');

$permisionOptions[] = funcForm::prepOption(1, 'View');
$permisionOptions[] = funcForm::prepOption(2, 'Add');
$permisionOptions[] = funcForm::prepOption(3, 'Edit');
$permisionOptions[] = funcForm::prepOption(4, 'Delete');

$userRoleId = funcArray::get($_GET, 'id');

$js = <<<JS
jQuery(document).ready(function($) {
  $('#btnAdd').click(function(e) {
    $('#txtAddRole').show();
    $('#btnSave').show();
    
  });
  $('.btnEdit').click(function(e) {
     var Id = this.id.substring(7);
     $("#txtOnly"+Id).hide();
     $("#txtBox"+Id).show();
     $('#btnSave').show();
  });
  $('#btnEditRole').click(function(e) {
     var Id = this.id.substring(7);
     $("#txtOnly"+Id).hide();
     $("#txtBox"+Id).show();
     $('#btnSave').show();
  });
});
JS;

$content .= funcForm::form('frmEditRoles', 'post') . funcForm::hidden('roleId', $userRoleId);
funcUI::queueScript('js', 'bottom', 'embed', $js);
$content .= '<div id="page-wrapper" style="background-color: #F5F5F5">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Roles</h1>
                    </div>
                </div>
            <table>
                <tr>
                    <td>' . funcForm::submit('btnSave', 'Save', 'btn btn-primary') . '</td>
                    <td style="padding-left:15px;">' . funcForm::text('txtAddRole', null, "form-control", null, null, 'style="display:none;" Placeholder=Enter&nbsp;Category') . '</td>
                    <td style="padding-left:15px;">' . funcForm::submit('btnSave', 'Save', 'btn btn-success', null, 'style="display:none;"') . '</td>
                </tr>
            </table>';
$modules = array ('Assets','Locations','Entity','Category','Status','Cost Centre','Conditions','Import Data','Users');
if ($modules) {
  $content .= '<table class="table table-striped table-bordered table-hover table-responsive" style="width:70%;">
                  <tr>
                      <th style="width:30%;">Module</th><th>Name</th>
                  </tr>';
  foreach ($modules as $key => $module) {
    $role = new Roles(array('RoleId' => $userRoleId, 'Module' => $module));
    $perms = array();
    if ($role->ViewRole == 1) {
      $perms[] = 1;
    }
    if ($role->AddRole== 1) {
      $perms[] = 2;
    }
    if ($role->EditRole == 1) {
      $perms[] = 3;
    }
    if ($role->DeleteRole == 1) {
      $perms[] = 4;
    }
    $perms = '0,' . implode(',', $perms);
//    echo funcArray::display($module . ' | ' . $perms);
    $content .= '<tr>
                      <td style="width:30%;">' . $module . '</td><td>' . funcForm::select('options[' . $key . '][]', null, null, $permisionOptions, 'select select2Multi form-control', false, 'multiple="multiple" style="width:300px" data-selected="' . $perms . '"', 'options' . $key) . '</td>
                  </tr>';
  }
}
$content .= '</table></div>';
$js = <<<JS
jQuery(document).ready(function($) {
    var selects = $('.select2Multi');

    $.each(selects,function(){
      var curSelect = '#' + $(this).attr("id");
      var selectedIds = $(curSelect).data('selected').toString();
      var newArray = [];
      if (selectedIds.indexOf(",") >= 0) {
        // alert(curSelect + ' | ' + selectedIds);
        var values = selectedIds.split(",");
        for(var i=0; i < values.length; i++){
            newArray.push(parseInt(values[i]));
        }
      }
      $(curSelect).val(newArray);
      $(curSelect).select2();
    });
});
JS;
funcUI::queueScript('js', 'bottom', 'embed', $js);
funcUI::queueScript('js', 'bottom', 'ext', 'js/select2.full.js');
funcUI::queueScript('css', 'top', 'ext', $GLOBALS['app.ui.theme.folder'] . 'css/select2.css');
$content .= funcForm::closeForm();