<?php
/**
 * Created by PhpStorm.
 * User: ken13
 * Date: 11/7/2017
 * Time: 6:24 PM
 */
funcCore::requireClasses('usertitle');
funcUI::queueScript('js', 'bottom', 'ext', 'js/jquery/jquery.js');
funcUI::queueScript('js', 'bottom', 'ext', 'js/jquery/jquery-ui.js');
funcUI::queueScript('js', 'bottom', 'ext', 'js/ui.script.js');
if ($GLOBALS['app.user']->IduserTitle != 5) {
  funcCore::redirect('home.php?module=index&action=index', 'You do not have permission to view this page.', $GLOBALS['app.alert.Error']);
}
$js = <<<JS
jQuery(document).ready(function($) {
  $('#btnAdd').click(function(e) {
    $('#txtAddRole').show();
    $('#btnSave').show();
    
  });
  $('.btnEdit').click(function(e) {
     var Id = this.id.substring(7);
     $("#txtOnly"+Id).hide();
     $("#txtBox"+Id).show();
     $('#btnSave').show();
  });
  $('.btnEditRole').click(function(e) {
     var Id = this.id.substring(11);
     window.location = 'home.php?module=roles&action=edit&id=' + Id;
  });
});
JS;

$roles = UserTitle::get(null, "`iduser_title` != 5");
//$titleOptions = funcArray::classesToSelectOptions($roles, 'IduserTitle', 'TitleName');
$content .= funcForm::form('frmRoles', 'post') . funcForm::hidden('deleteId', null);
funcUI::queueScript('js', 'bottom', 'embed', $js);
$content .= '<div id="page-wrapper" style="background-color: #F5F5F5">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Roles</h1>
                    </div>
                </div>
            <table>
                <tr>
                    <td>' . funcForm::button('btnAdd', 'Add Role', 'btn btn-primary') . '</td>
                    <td style="padding-left:15px;">' . funcForm::text('txtAddRole', null, "form-control", null, null, 'style="display:none;" Placeholder=Enter&nbsp;Role') . '</td>
                    <td style="padding-left:15px;">' . funcForm::submit('btnSave', 'Save', 'btn btn-success', null, 'style="display:none;"') . '</td>
                </tr>
            </table>';
if ($roles) {
  $content .= '<table class="table table-striped table-bordered table-hover table-responsive" style="width:70%;">
                    <tr>
                        <th>Action</th>
                        <th>Name</th>
                    </tr>';
  foreach ($roles as $role) {
    $content .= '<tr>
                        <td style="padding-left:15px;">' . funcForm::button('btnEdit' . $role->IduserTitle, 'Edit Name', "btnEdit form-control btn btn-info", null, 'style="width:20%;"') . '
                          <span style="margin-left:1xtx;">' . funcForm::button('btnEditRole' . $role->IduserTitle, 'Edit Role', "btnEditRole form-control btn btn-success", null, 'style="width:20%;"') . '</span>
                          <span style="margin-left:1xtx;">' . funcForm::button('btnDelete', 'Delete', "form-control btn btn-danger", null, 'style="width:20%;"') . '</span>
                         </td>
                        <td>
                            <span id="txtOnly' . $role->IduserTitle . '">' . $role->TitleName . '</span>
                            <span id="txtBox' . $role->IduserTitle . '" style="display:none;">' . funcForm::text('txtUpdate[' . $role->IduserTitle . ']', $role->TitleName, "form-control", null, null) . '</span>
                        </td>
                    </tr>';
  }
}
$content .= '</table></div>';
