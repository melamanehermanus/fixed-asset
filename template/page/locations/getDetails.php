<?php
funcCore::requireClasses('location');

$locationId = funcArray::get($_REQUEST, 'locationId');

$locationdetails = Location::get(null, "`idlocation` = {$locationId}");

if (!empty($locationdetails)) {
  $locationdetails = funcArray::getFirstItem($locationdetails);
  echo '
      
        <tr>
            <th style="height:40px; color: #FFFFFF;background-color: #15147b">Location Name</th>
            <th style="color: #FFFFFF;background-color: #15147b">Location QR Code</th>
            <th style="color: #FFFFFF;background-color: #15147b">Building</th>
            <th style="color: #FFFFFF;background-color: #15147b">Region</th>
            <th style="color: #FFFFFF;background-color: #15147b">Country</th>
            <th style="color: #FFFFFF;background-color: #15147b">Longitude</th>
            <th style="color: #FFFFFF;background-color: #15147b">Latitude</th>
           </tr>        
            <tr>
            <td>' . $locationdetails->LocationName . '</td>
             <td>' . $locationdetails->Barcode . '</td> 
             <td>' . $locationdetails->Building . '</td>
               <td>' . $locationdetails->Region . '</td>
                <td>' . $locationdetails->Country . '</td>
                <td>' . $locationdetails->Longitude . '</td>
            <   td>' . $locationdetails->Latitude . '</td>
            </tr>';



//  <div style="margin-top: 15px;">
//    <table class="table table-striped table-bordered table-hover table-responsive">
//      <tr>
//        <th style="color: #15147b;">Location Name</th>
//        <td>' . $locationdetails->LocationName . '</td>
//        <th style="color: #15147b;">Building</th>
//        <td>' . $locationdetails->Building . '</td>
//        <th style="color: #15147b;">Region</th>
//        <td>' . $locationdetails->Region . '</td>
//        <th style="color: #15147b;">Country</th>
//        <td>' . $locationdetails->Country . '</td>
//      </tr>
//      <tr>
//      <th style="color: #15147b;">Barcode</th>
//      <td>' . $locationdetails->Barcode . '</td>
//      <th style="color: #15147b;">Longitude</th>
//      <td>' . $locationdetails->Longitude . '</td>
//      <th style="color: #15147b;">Latitude</th>
//      <td>' . $locationdetails->Latitude . '</td>
//      <td colspan="2">&nbsp;</td></tr>
//    </table>  
//  </div>';
}
exit;

?>
