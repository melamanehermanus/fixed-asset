<?php
funcCore::requireClasses('user, usertitle, entity');
require_once($GLOBALS['app.folder.include'] . 'class.pagination.php');
if (!isset($GLOBALS['app.var.user.permissions']['Users']['view']) || $GLOBALS['app.var.user.permissions']['Users']['view'] == 0) {
  funcCore::redirect('home.php?module=index&action=index', 'You do not have permission to view this page.', $GLOBALS['app.alert.Error']);
}
$entityId = funcArray::get($_POST, 'ddEntity');

$user = User::get(null, null, 'Iduser ASC',($GLOBALS['app.user']->IduserTitle == 5 ? "left join entity on entity.identity = user.identity" : "inner join entity on entity.identity = user.identity"));
$entities = Entity::get();
$entityOptions = funcArray::classesToSelectOptions($entities, 'Identity', 'EntityName');

$countUser = count($user);

$content .= funcForm::form('frmUser', 'post');

$content .= '<div id="page-wrapper" style="background-color: #F5F5F5">
                <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">User Titles</h1>
                </div>
            </div>';
            if ($GLOBALS['app.var.user.permissions']['Users']['add'] == 1) {
              $content .= '<table>
                <tr>
                    <td><a class="btn btn-primary" href="home.php?module=user&action=modify&ddEntity=' . $entityId . '"><i class="fa fa-plus"></i> Add user</a></td>
                </tr>
                <tr><td></td></tr>
            </table>';
            }
            $content .= '<table>
                <tr>
                    <td>Entity ' . funcForm::select('ddEntity', $entityId, 'View All', $entityOptions, "form-control") . '</td>
                    <td style="padding-left:15px;">&nbsp;' . funcForm::submit('btnSearch', 'Search', "btn btn-primary form-control") . '<td>
                </tr>
            </table>
            <h1>' . $countUser . ' users</h1>
    <table id="maintable" class="table table-striped table-bordered table-hover table-responsive">
        <tr>';
          if ($GLOBALS['app.var.user.permissions']['Users']['edit'] == 1) {
            $content .= '<th>Action</th>';
          }
            $content .= '
            <th>User Name</th>
            <th>Email</th>
            <th>Contact Number</th>
            <th>Entity</th>
            <th>Title</th>
        </tr>';

foreach ($user as $u) {
    $title = new UserTitle($u->IduserTitle);
    $content .= '
        <tr>';
          if ($GLOBALS['app.var.user.permissions']['Users']['edit'] == 1) {
            $content .= '<td><a class="btn btn-info" href="home.php?module=user&action=modify&id=' . $u->Iduser. '"><i class="fa fa-edit"></i> Modify</a></td>';
          }
          $content .= '
            <td>' . $u->UserName . '</td>
            <td>' . $u->Email . '</td>
            <td>' . $u->ContactNumber . '</td>
            <td>' . (!empty($u->Identity) ? $entities[$u->Identity]->EntityName : null) . '</td>
            <td>' . $title->TitleName . '</td>
            
        </tr>
    ';
}
$content .= '</table>' . funcForm::closeForm(). '</div>';

?>