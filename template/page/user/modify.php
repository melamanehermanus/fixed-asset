<?php
funcCore::requireClasses('user,usertitle,entity');
if (!isset($GLOBALS['app.var.user.permissions']['Users']['edit']) || $GLOBALS['app.var.user.permissions']['Users']['edit'] == 0) {
  funcCore::redirect('home.php?module=index&action=index', 'You do not have permission to view this page.', $GLOBALS['app.alert.Error']);
}

$userId = funcArray::get($_GET, 'id');
$entityId = funcArray::get($_GET, 'ddEntity');
$entityOptions = funcArray::classesToSelectOptions(Entity::get(), 'Identity', 'EntityName');
$titleOptions = funcArray::classesToSelectOptions(UserTitle::get(null, ($GLOBALS['app.user']->IduserTitle == 5 ? null : "iduser_title != 5")), 'IduserTitle', 'TitleName');
//echo funcArray::display($_REQUEST);
//exit;
$user = new User($userId);
$lookupResult = $user->lookup($userId);

$js = <<<JS
jQuery(document).ready(function($) {
  $('#btnBack').click(function(e) {
    window.location = 'home.php?module=user&action=index';
  });
  $('#btnDelete').click(function(e) {
    window.location = 'home.php?module=user&action=delete&method=script&id={$userId}';
  });
});
JS;
funcUI::queueScript('js', 'bottom', 'embed', $js);

funcForm::validation('frmUser');
$content .=  funcForm::form('frmUser') .
              funcForm::hidden('module', 'user') .
              funcForm::hidden('action', 'save') .
              funcForm::hidden('Id', $user->Iduser) .
             '<div id="page-wrapper" style="background-color: #F5F5F5">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">User Modificaiton</h1>
                    </div>
                </div>
             <table style="width:50%">
                <tr>
                    <th>User Name</th>
                    <td>' . funcForm::text('UserName', $user->UserName, "input validate['required'] form-control") . '</td></tr>
                <tr>
                    <th>Password</th>
                    <td>' . funcForm::password('Password', null, "input form-control") . '</td></tr>
                <tr>
                    <th>Confirm Password</th>
                    <td>' . funcForm::password('Password2', null, "input validate['confirm[Password]'] form-control") . '</td></tr>
                <tr>
                    <th>Contact Number</th>
                    <td>' . funcForm::text('ContactNumber', $user->ContactNumber, "input validate['required'] form-control") . '</td></tr>
                <tr>
                    <th>Email</th>
                    <td>' . funcForm::text('txtEmail', $user->Email, "form-control") . '</td></tr>
                <tr>
                    <th>Entity</th>
                    <td>' . funcForm::select('ddEntity', $user->Identity, 'Please Select', $entityOptions, "form-control") . '</td></tr>
                <tr>
                    <th>Type</th>
                    <td>' . funcForm::select('ddTitle', $user->IduserTitle, 'Please Select', $titleOptions, "select form-control") . '</td>
                </tr>';
            
$content .= '<tr>
                <td align="right" colspan="2">
                ' . ($GLOBALS['app.var.user.permissions']['Users']['delete'] == 1 ? funcForm::button('btnDelete', 'Delete', "btn btn-danger", true) : null) .
                '&nbsp&nbsp' .
                funcForm::button('btnBack', 'Back', "btn btn-default", true) .
                '&nbsp&nbsp' .
                funcForm::submit('btnSubmit', 'Save', "btn btn-primary", true) .
                '</td>
            </tr>
        </table>' . funcForm::closeForm();
?>