<?php
funcCore::requireClasses('costcentre');
funcUI::queueScript('js', 'bottom', 'ext', 'js/jquery/jquery.js');
funcUI::queueScript('js', 'bottom', 'ext', 'js/jquery/jquery-ui.js');
funcUI::queueScript('js', 'bottom', 'ext', 'js/ui.script.js');
if (!isset($GLOBALS['app.var.user.permissions']['Cost Centre']['view']) || $GLOBALS['app.var.user.permissions']['Cost Centre']['view'] == 0) {
  funcCore::redirect('home.php?module=index&action=index', 'You do not have permission to view this page.', $GLOBALS['app.alert.Error']);
}
$js = <<<JS
jQuery(document).ready(function($) {
  $('#btnAdd').click(function(e) {
    $('#txtAddCostCentre').show();
    $('#btnSave').show();
    
  });
  $('.btnEdit').click(function(e) {
     var Id = this.id.substring(7);
     $("#txtOnly"+Id).hide();
     $("#txtBox"+Id).show();
     $('#btnSave').show();
  });

  $('.btnDelete').click(function(e) {
    if (confirm('Are you sure you would like to delete this item?')) {
      var Id = this.id.substring(9);
      $('#deleteId').val(Id);
    }
    else {
      return false;
    }
  });
});
JS;

$costcentre = Costcentre::get();

funcUI::queueScript('js', 'bottom', 'embed', $js);
$content .= funcForm::form('frmCondition', 'post') . funcForm::hidden('deleteId', null);
$content .= '<div id="page-wrapper" style="background-color: #F5F5F5">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Cost Centre</h1>
                    </div>
                </div>
            <table>
                <tr>';
if ($GLOBALS['app.var.user.permissions']['Cost Centre']['add'] == 1) {
  $content .= '<td>' . funcForm::button('btnAdd', 'Add Cost Centre', 'btn btn-primary') . '</td>
                <td style="padding-left:15px;">' . funcForm::text('txtAddCostCentre', null, "form-control", null, null, 'style="display:none;" Placeholder=Enter&nbsp;Cost&nbsp;Centre') . '</td>
                <td style="padding-left:15px;">' . funcForm::submit('btnSave', 'Save', 'btn btn-success', null, 'style="display:none;"') . '</td>';
}
$content .= '</tr>
            </table>';
if ($costcentre) {
  $showAction = ($GLOBALS['app.var.user.permissions']['Cost Centre']['edit'] == 0 && $GLOBALS['app.var.user.permissions']['Cost Centre']['delete'] == 0 ? false : true);
  $content .= '<table class="table table-striped table-bordered table-hover table-responsive" style="width:70%;">
                    <tr>';
                      if ($showAction) {
                        $content .= '<th>Action</th>';
                      }
                      $content .= '<th>Name</th>
                    </tr>';
    foreach ($costcentre as $cc) {
        $content .= '<tr>';
                      if ($showAction) {
                        $content .= '<td style="padding-left:15px;">' . ($GLOBALS['app.var.user.permissions']['Cost Centre']['edit'] == 1 ? funcForm::button('btnEdit' . $cc->IdcostCentre, 'Edit', "btnEdit form-control btn btn-info", null, 'style="width:30%;"') : null) . ($GLOBALS['app.var.user.permissions']['Cost Centre']['delete'] == 1 ? '<span style="margin-left:15px;">' . funcForm::submit('btnDelete' . $cc->IdcostCentre, 'Delete', "btnDelete form-control btn btn-danger", null, 'style="width:30%;"') . '</span>' : null) . '</td>';
                      }
      $content .= '<td>
                            <span id="txtOnly' . $cc->IdcostCentre . '">' . $cc->CostCentre . '</span>
                            <span id="txtBox' . $cc->IdcostCentre . '" style="display:none;">' . funcForm::text('txtUpdate[' . $cc->IdcostCentre . ']' , $cc->CostCentre, "form-control", null, null) . '</span></td>
                    </tr>';    
    }    
}
$content.='</table></div>' . funcForm::closeForm();
?>