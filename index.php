<?php
require_once('include/setup.app.php');
if (funcUser::loggedIn() && isset($GLOBALS['app.var.user.redirectafterlogin']) && !empty($GLOBALS['app.var.user.redirectafterlogin'])) {
  funcCore::redirect($GLOBALS['app.var.user.redirectafterlogin']);
}

$content .= funcUI::getPage('index.php');

funcUI::renderOutput($GLOBALS['app.ui.theme'], 'html.php');
?>