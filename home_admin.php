<?php
require_once('include/setup.app.php');

funcUser::requireLogin();

if (empty($module)) {
  $module = 'index';
}
if (empty($action)) {
  $action = 'index';
}

$initUseCache = $GLOBALS['app.db.usecache'];
$menu = funcUI::getPage('navigation_admin.php', 'index');
if (count($_POST) > 0 || $method == 'script') {
  $GLOBALS['app.db.usecache'] = false;
  $content .= funcUI::runScript($action . '.php', $module);
  $GLOBALS['app.db.usecache'] = $initUseCache;
}
if ($module == 'cron') {
  $GLOBALS['app.db.usecache'] = false;
}
$page = funcUI::getPage($action . '.php', $module);
if ($module == 'cron') {
  $GLOBALS['app.db.usecache'] = $initUseCache;
}
if ($page === false) {
  funcCore::redirect('home_admin.php', 'Page does not exist');
}
if(!isset($_GET['ajax'])){
  $content .= $menu;
}
$content .= $page;
unset($page);

funcUI::renderOutput($GLOBALS['app.ui.theme'], 'html.php');
?>