<?php
require_once('include/setup.app.php');
$htmlTable = '<table>
                <tr>
                  <td></td>
                  <td excelFormat="aw">First Item</td>
                  <td excelFormat="aw">Second Item</td>
                  <td excelFormat="aw">Third Item</td>
                  <td excelFormat="aw">Fourth Item</td>
                  <td excelFormat="aw">Fifth Item</td>
                </tr>
              </table>';
$tabindex = 0;
include $GLOBALS['app.folder.include'] . 'PHPExcel.php';
include $GLOBALS['app.folder.include'] . 'PHPExcel/Writer/Excel5.php';

$objPHPExcel = new PHPExcel();
$objPHPExcel->getProperties()->setCreator("Facet Asset Management");
$objPHPExcel->getProperties()->setLastModifiedBy("Facet Asset Management");
$objPHPExcel->getProperties()->setTitle("Facet Asset Management");
$objPHPExcel->getProperties()->setSubject("Facet Asset Management");
$objPHPExcel->getProperties()->setDescription("Facet Asset Management");

$objPHPExcel->setActiveSheetIndex($tabindex);
$objPHPExcel->getActiveSheet()->setTitle('Asset Details');
funcData::html2xls($htmlTable, $objPHPExcel, $tabindex, 80);

//Add the image to the cell
$objDrawing = new PHPExcel_Worksheet_Drawing();
$objDrawing->setName('Test');
$objDrawing->setDescription('Test');
$objDrawing->setPath($imagePath);
$objDrawing->setResizeProportional(true);
$objDrawing->setWidth(60);
$objDrawing->setCoordinates('A1');
$objDrawing->setWorksheet($objPHPExcel->getActiveSheet());

header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="asset_details_' . urlencode(date('Y-m-d_H-i-s') . '.xls') . '"');
header('Content-Transfer-Encoding: binary');
header('Expires: 0');

// check for IE only headers
if (isset($_SERVER['HTTP_USER_AGENT']) && (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE') !== false)) {
  header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
  header('Pragma: public');
}
else {
  header('Pragma: no-cache');
}

$objWriter = new PHPExcel_Writer_Excel5($objPHPExcel);
$objWriter->save('php://output');
exit;
?>