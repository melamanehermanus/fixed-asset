<?php
error_reporting(E_ALL);
//include 'fileUpload.php';
require_once 'config.php';


if(isset($_POST['function'])){
    if($_POST['function'] === "getAssetDetails"){
            
        if(isset($_POST['barcode'])){
            
            $mysqli = getmysqli();

            $barcode = $_POST['barcode'];

            $result = getAssetDetails($barcode);

            if($result->num_rows > 0) {
                $assetArray = array();
                        while($row = $result->fetch_array(MYSQL_ASSOC)) {
                            //echo "in while";
                            $assetArray[] = $row;
                        }

                $resultsArray = array('result'=>'success','results'=>$assetArray);

                        echo json_encode($resultsArray);

                } else {
                    $result = array(
                            "result" => "empty"
                        );
                        echo json_encode($result);
                }
                //mysqli_close($connection); // Closing Connection
            $mysqli->close();
        }
            
        
    }elseif($_POST['function'] === "getAssetPhotos"){
            
        if(isset($_POST['idasset'])){
            
            $mysqli = getmysqli();

            $idAsset = $_POST['idasset'];

            $result = getAssetPhotos($idAsset,$_POST['get_all']);

            if($result->num_rows > 0) {
                $assetPhotosArray = array();
                        while($row = $result->fetch_array(MYSQL_ASSOC)) {
                            //echo "in while";
                            $assetPhotosArray[] = $row;
                        }

                $resultsArray = array('result'=>'success','results'=>$assetPhotosArray);

                        echo json_encode($resultsArray);

                } else {
                    $result = array(
                            "result" => "empty"
                        );
                        echo json_encode($result);
                }
                //mysqli_close($connection); // Closing Connection
            $mysqli->close();
        }
            
        
    }elseif($_POST['function'] === "insertAsset"){
            
//        if(isset($_POST['location_id']) && 
//                isset($_POST['asset_barcode'])  && 
//                isset($_POST['asset_description'])  && 
//                isset($_POST['asset_number'])  && 
//                isset($_POST['asset_serial_number'])  && 
//                isset($_POST['asset_make'])  && 
//                isset($_POST['condition'])  && 
//                isset($_POST['status'])  && 
//                isset($_POST['costcenter'])  && 
//                isset($_POST['category'])){
           
         
            $result = getAssetDetails($_POST['asset_barcode']);
            if($result->num_rows > 0) {
                
                $result = array(
                            "result" => "This asset already exists"
                        );
                        echo json_encode($result);
                
            }
        
        
        
        
            $result = insertAsset($_POST['location_id'],
                $_POST['asset_barcode'],
                $_POST['asset_description'],
                $_POST['asset_number'],
                $_POST['asset_serial_number'],
                $_POST['asset_make'],
                $_POST['asset_model'],
                $_POST['condition'],
                $_POST['status'],
                $_POST['costcenter'],
                $_POST['category'],
                $_POST['iduser'],
                $_POST['longitude'],
                $_POST['latitude']);

        
            
            //echo $result;
            
            if($result <> 0) {
                
                        $assetId = $result;
                
                        $imageCount = $_POST['image_count'];
                        $index = 0;
                        while($index < $imageCount) {
                            $priority = "0";
                            if($_POST['priority_image_index'] == $index)
                               $priority = "1";
                            else
                               $priority = "0"; 
                            //echo $_FILES[$index]['name'];
                            //echo $_FILES[$index]['tmp_name'];
                            $response = saveImage(basename($_FILES[$index]['name']),$_FILES[$index]['tmp_name'],$_POST['identity']);
                            
                            if ($response['error'] == true){  
                                $result = 'error uploading photo.';
                                //$response['message'] = 'Error saving file path!';
                                //echo json_encode($response);    
                            }else{
                                $result = insertAssetImagePath ($assetId,$response['file_path'],$priority); 
                                if($result = 1) {
                                    $result = 'success';
                                }else{
                                    $result = 'error creating new asset.';
                                }
                            }
                            $index++;
                        } 
                        if($result === true) {
                            $result = 'success';
                         }
                        $resultsArray = array('result'=>$result);
                        echo json_encode($resultsArray);

                } else {
                    $result = array(
                            "result" => "error inserting asset" . $result
                        );
                        echo json_encode($result);
                }
                //mysqli_close($connection); // Closing Connection

            //$mysqli->close();
            
    }elseif($_POST['function'] === "updateAsset"){
                       
            $assetId = $_POST['asset_id'];
            
            $result = updateAsset($_POST['location_id'],               
                $_POST['asset_id'],
                $_POST['asset_barcode'],
                $_POST['asset_description'],
                $_POST['asset_number'],
                $_POST['asset_serial_number'],
                $_POST['asset_make'],
                $_POST['asset_model'],
                $_POST['condition'],
                $_POST['status'],
                $_POST['costcenter'],
                $_POST['category'],
                $_POST['iduser'],
                $_POST['longitude'],
                $_POST['latitude']);

        
            //echo $result;
            
            if($result <> 0) {
                
                        $assetId = $result;
                
                        $imageCount = $_POST['image_count'];
                        $index = 0;
                        while($index < $imageCount) {
                            $priority = "0";
                            if($_POST['priority_image_index'] == $index)
                               $priority = "1";
                            else
                               $priority = "0"; 
                            //echo $_FILES[$index]['name'];
                            //echo $_FILES[$index]['tmp_name'];
                            $response = saveImage(basename($_FILES[$index]['name']),$_FILES[$index]['tmp_name'],$_POST['identity']);
                            
                            if ($response['error'] == true){  
                                $result = 'error uploading photo.';
                                //$response['message'] = 'Error saving file path!';
                                //echo json_encode($response);    
                            }else{
                                $result = insertAssetImagePath ($assetId,$response['file_path'],$priority); 
                                if($result = 1) {
                                    $result = 'success';
                                }else{
                                    $result = 'error creating new asset.';
                                }
                            }
                            $index++;
                        } 
                        
                         if($result <> 0) {
                            $result = 'success';
                         }
                        $resultsArray = array('result'=>$result);
                        echo json_encode($resultsArray);

                } else {
                    $result = array(
                            "result" => "error updating asset" . $result
                        );
                        echo json_encode($result);
                }
                //mysqli_close($connection); // Closing Connection

            //$mysqli->close();
            
    }
                                                 
}


function insertAssetImagePath($assetId, $imageUrl, $imagePriority){
    
    $mysqli = getmysqli();

    $date = date("Y-m-d");
    
    $sql =  "INSERT INTO `Fixed_Asset`.`asset_image`
                (`idasset`,
                `asset_url`,
                `image_priority`,
                `image_date`)
                VALUES
                ('$assetId',
                '$imageUrl',
                $imagePriority,
                '$date')";
    
    $result = $mysqli->query($sql) or trigger_error($mysqli->error." [$sql]");
    return $result;
        //echo "New record created successfully";
    
    $mysqli->close();
}

function insertAsset($location_id,$asset_barcode,$asset_description,$asset_number,$asset_serial_number,$asset_make,$asset_model,$condition,$status,$costcenter,$category,$iduser,$longitude,$latitude) {
  
    $mysqli = getmysqli();
    
    $sql = "INSERT INTO Fixed_Asset.asset 
            (`idlocation`,
            `asset_barcode`,
            `asset_number`,
            `category_name`,
            `make`,
            `model`,
            `serial_number`,
            `iduser`,
            `idcost_centre`,
            `idcondition`,
            `idstatus`,
            `asset_description`,
            `longitude`,
            `latitude`)
            VALUES
            ((select idlocation from Fixed_Asset.location where location_name = '$location_id'),
            '$asset_barcode',
            '$asset_number',
            '$category',
            '$asset_make',
            '$asset_model',
            '$asset_serial_number',
            '$iduser',
            (select Fixed_Asset.cost_centre.idcost_centre from Fixed_Asset.cost_centre where cost_centre = '$costcenter'),
            (select Fixed_Asset.condition.idcondition from Fixed_Asset.condition where condition_name = '$condition'),
            (select Fixed_Asset.status.idstatus from Fixed_Asset.status where status_name = '$status'),
            '$asset_description',
            '$longitude',
            '$latitude')";

    $result = $mysqli->query($sql) or trigger_error($mysqli->error." [$sql]"); 
    $id = mysqli_insert_id($mysqli);
    if($result==1)
        return $id;
    else
        return $result;
    
    $mysqli->close();
 
}

function updateAsset($location_id,$assetId,$asset_barcode,$asset_description,$asset_number,$asset_serial_number,$asset_make,$asset_model,$condition,$status,$costcenter,$category,$iduser,$longitude,$latitude) {
  
    $mysqli = getmysqli();
        
    $sql = "UPDATE `Fixed_Asset`.`asset`
            SET
            `idlocation` = (select idlocation from Fixed_Asset.location where location_name = '$location_id'),
            `asset_barcode` = '$asset_barcode',
            `asset_number` = '$asset_number',
            `category_name` = '$category',
            `asset_description` = '$asset_description',
            `make` = '$asset_make',
            `model` = '$asset_model',
            `serial_number` = '$asset_serial_number',
            `iduser` = '$iduser',
            `idcost_centre` = (select Fixed_Asset.cost_centre.idcost_centre from Fixed_Asset.cost_centre where cost_centre = '$costcenter'),
            `idcondition` = (select Fixed_Asset.condition.idcondition from Fixed_Asset.condition where condition_name = '$condition'),
            `idstatus` = (select Fixed_Asset.status.idstatus from Fixed_Asset.status where status_name = '$status'),
            `longitude` = '$longitude',
            `latitude` = '$latitude'
            WHERE `idasset` = '$assetId';";
            

    $result = $mysqli->query($sql) or trigger_error($mysqli->error." [$sql]"); 
    $id = mysqli_insert_id($mysqli);
    if($result==1)
        return $assetId;
    else
        return $result;
    
    $mysqli->close();
 
}
      
function getAssetPhotos($idAsset,$getAll) {
  
    
    $mysqli = getmysqli();
    
    if($getAll == "no")
        $sql = "SELECT * FROM Fixed_Asset.asset_image WHERE idasset = '$idAsset' AND image_priority = 1";
    else
        $sql = "SELECT * FROM Fixed_Asset.asset_image WHERE idasset = '$idAsset'";
    
    //return $sql;
    $result = $mysqli->query($sql) or trigger_error($mysqli->error." [$sql]"); 
    return $result;
    $mysqli->close();
 
}    
    
    
function getAssetDetails($barcode) {
  
    $mysqli = getmysqli();
    

    
    $sql = "SELECT `asset`.`idasset`,
            `asset`.`idlocation`,
            `asset`.`asset_barcode`,
            `asset`.`asset_number`,
            `asset`.`category_name`,
            `asset`.`make`,
            `asset`.`model`,
            `asset`.`serial_number`,
            `asset`.`iduser`,
            `asset`.`idcost_centre`,
            `asset`.`idcondition`,
            `asset`.`idstatus`,
            `asset`.`latitude`,
            `asset`.`longitude`,
            `asset`.`asset_description`,
            cost_centre.cost_centre, 
            condition.condition_name,
            status.status_name,
            location.location_name 
            FROM Fixed_Asset.asset,
            Fixed_Asset.cost_centre,
            Fixed_Asset.condition,
            Fixed_Asset.status,
            Fixed_Asset.location
            WHERE Fixed_Asset.location.idlocation = Fixed_Asset.asset.idlocation
            AND Fixed_Asset.cost_centre.idcost_centre = Fixed_Asset.asset.idcost_centre
            AND Fixed_Asset.condition.idcondition = Fixed_Asset.asset.idcondition
            AND Fixed_Asset.status.idstatus = Fixed_Asset.asset.idstatus
            AND  Fixed_Asset.asset.asset_barcode = '$barcode'";
    
    //return $sql;
    $result = $mysqli->query($sql) or trigger_error($mysqli->error." [$sql]"); 
    return $result;
    $mysqli->close();
 
}

function saveImage($fileName,$fileNameTemp,$entityId){
    
$target_path = "../asset_images/";
//$server_ip = gethostbyname('www.gradeacademy.co.za');
//$server_ip = gethostbyname('jabulisa.eduze.com');

//$hostname="www115.jnb2.host-h.net";
// final file url that is being uploaded
//$file_upload_url = 'http://' . $server_ip . "/images/profile_images/";
 
    $target_path = $target_path . $fileName;
    
    $response['file_name'] = $fileName;
 
    try {
        // Throws exception incase file is not being moved
        if (!move_uploaded_file($fileNameTemp, $target_path)) {
            // make error flag true
            $response['error'] = true;
            $response['message'] = 'Could not move the file!';
            return $response;
        }else{
        // File successfully uploaded
            $response['message'] = 'File uploaded successfully!';
            $response['error'] = false;
            $response['file_path'] =  "/asset_images/" . $fileName;
            return $response;
            //echo json_encode($response);
            //$response['file_path'] = $file_upload_url . basename($_FILES['image']['name']);
            //$response['file_path'] = $target_path . basename($_FILES['image']['name']);
        }
    } catch (Exception $e) {
        // Exception occurred. Make error flag true
        $response['error'] = true;
        $response['message'] = $e->getMessage();
    }
    return $response;
    //echo json_encode($response);
    
}
?>